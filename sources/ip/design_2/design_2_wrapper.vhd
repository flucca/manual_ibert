--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Fri Jul 29 09:42:07 2022
--Host        : celeste.phy.bnl.gov running 64-bit CentOS Linux release 8.2.2004 (Core)
--Command     : generate_target design_2_wrapper.bd
--Design      : design_2_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_2_wrapper is
  port (
    addr_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_in : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 63 downto 0 );
    data_vio_o : out STD_LOGIC_VECTOR ( 63 downto 0 );
    ram_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ram_din : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ram_dout : out STD_LOGIC_VECTOR ( 63 downto 0 );
    ram_en : in STD_LOGIC;
    ram_rst : in STD_LOGIC;
    ram_src_slct_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    ram_we : in STD_LOGIC_VECTOR ( 7 downto 0 );
    reset_in : in STD_LOGIC;
    we_o : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end design_2_wrapper;

architecture STRUCTURE of design_2_wrapper is
  component design_2 is
  port (
    reset_in : in STD_LOGIC;
    we_o : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ram_src_slct_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_in : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 63 downto 0 );
    data_vio_o : out STD_LOGIC_VECTOR ( 63 downto 0 );
    addr_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ram_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ram_din : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ram_dout : out STD_LOGIC_VECTOR ( 63 downto 0 );
    ram_en : in STD_LOGIC;
    ram_rst : in STD_LOGIC;
    ram_we : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  end component design_2;
begin
design_2_i: component design_2
     port map (
      addr_o(15 downto 0) => addr_o(15 downto 0),
      clk_in => clk_in,
      data_i(63 downto 0) => data_i(63 downto 0),
      data_vio_o(63 downto 0) => data_vio_o(63 downto 0),
      ram_addr(31 downto 0) => ram_addr(31 downto 0),
      ram_din(63 downto 0) => ram_din(63 downto 0),
      ram_dout(63 downto 0) => ram_dout(63 downto 0),
      ram_en => ram_en,
      ram_rst => ram_rst,
      ram_src_slct_o(0) => ram_src_slct_o(0),
      ram_we(7 downto 0) => ram_we(7 downto 0),
      reset_in => reset_in,
      we_o(3 downto 0) => we_o(3 downto 0)
    );
end STRUCTURE;
