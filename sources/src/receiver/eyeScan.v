//-----------------------------------------------------------------------------
// Title      : Eye Scan FSM                                            
// Project    : Eye Scan RTL                                                     
//-----------------------------------------------------------------------------
// File       : eyeScan.v                                          
//-----------------------------------------------------------------------------
// Description: This file contains the 
// 10GBASE-R clocking and reset logic which can be shared between multiple cores                
//-----------------------------------------------------------------------------
// (c) Copyright 2009 - 2013 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and 
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// Version .9    3/31/2015
 module eyeScan
    (
                          input             CLK,
     (* keep = "true" *)  input             TXFSMRESETDONE,
     (* keep = "true" *)  input             RXFSMRESETDONE,
                          output reg       eyescanreset_o,
                          output reg        reset_gt_o,
                          output reg [2:0] rate_o,
                          input wire        reset_dn_i,
                          output wire       rxlpmen_o, //1 = lpm, 0 dfe
     (* keep = "true" *)  output wire       SOFRST,
    (* keep = "true" *)   input wire [15:0] DRPDO,
    (* keep = "true" *)   input wire        DRPRDY,
    (* keep = "true" *)   output reg [9:0]  DRPADDR,
    (* keep = "true" *)   output reg        DRPWE,
    (* keep = "true" *)   output reg        DRPEN,
    (* keep = "true" *)   output reg [15:0] DRPDI,
                          output wire [1:0] gty_select
    );
   
    reg [7:0] state;
    reg setRun = 0;
    reg [3:0]   status;
    reg [15:0]  errors,errorsOutput;
    reg [8:0]   hsteps,numSteps_reg = 64;
    reg [8:0]   vsteps,rate = 64;
    reg [11:0]  hstepSize = 1;
    reg [5:0]   cycles;
    reg [6:0]  vstepSize = 4;
    reg [5:0]   cnt;
    reg [10:0]  horz;
    reg [6:0]  vert;
    reg [15:0]  samples,samplesOutput;
    reg         ilaClk,initState;
    reg [9:0]   drpAddr;
    reg         drpWe;
    reg         drpEn;
    reg [15:0]  drpDi;
    wire        rstRtl;
    reg         drpDone;
    reg [1:0] uDM = 0;
    reg [1:0] vs_range = 3;
    wire [3:0] vs_rangeWire;
    reg       vs_neg = 0;
    reg       log2;
    reg       posDn = 0;
    reg       vs_ut_sign = 0;
    reg [2:0] go_reg,v_go;
    reg [1:0] raux;
    reg [4:0] PRESCALE;
    reg [6:0] numVsteps;
    reg [3:0] laneDeskew = 4'hF;
    reg [15:0]   vertOutput,horzOutput;
    reg       incVert = 0;
    reg [4:0] vertMsb;
    wire [4:0] prescaleWire;
    (*keep = "true" *)   wire[8:0] numSteps;
 (* keep = "true" *)   wire eyescanrst_in;
 (* keep = "true" *)   wire [9:0]  drpaddrReg;  // output wire [8 : 0] probe_out0
 (* keep = "true" *)   wire [15:0] drpdiReg;  // output wire [14 : 0] probe_out1
 (* keep = "true" *)   wire        drpEnCmd;  // output wire [0 : 0] probe_out2
 (* keep = "true" *)   wire        drpWeCmd;  // output wire [0 : 0] probe_out3
 (* keep = "true" *)   wire[11:0]  hstepSize_v;
 (* keep = "true" *)   wire[11:0]  vstepSize_v;
 (* keep = "true" *)   wire[1:0]   go;
 (* keep = "true" *)   reg   trig;
// (* keep = "true" *)   wire[2:0]   rate_v;
    parameter rate_v = 1;
    parameter waitGoCmd=0, run = 1, waitStart = 2, waitComplete = 3,getErrors=4, storeErrors=5,getSamples=6,storeSamples=7,rstCmd=8,waitRstCmd=9,
              updateHorizontal=10,updateVertical =11, waitRun = 12, waitHorizontal=13, waitVertical=14,testStart=15,testComplete = 16,
              storeData = 17, init = 18, updateDataMask = 19, updateQualMask = 20, setOffsets = 21, setVertOffset = 22, waitVert = 23, 
              setHorzOffset = 24, OffsetDn = 25, reset = 26, realign = 27, waitInit = 28, readHorzReg = 30, waitReadHorz = 31, checkRState = 32, 
              waitRState = 33, readVertReg = 34, waitReadVert = 35;
    parameter ES_HORZ_OFFSET = 10'h04f, CONTROL = 10'h03C, CONTROL_STATUS = 10'H253, SAMPLE_COUNT = 10'H252, ERROR_COUNT = 10'H251, 
              DATA_MASK0 = 10'H049, DATA_MASK5 = 10'H0F1, QUAL_MASK5 = 10'h0EC, QUAL_MASK0 = 10'h044, VS_REG = 10'h097;
    parameter maxVertSteps = 7'b1111111;
    
//    always @ (posedge CLK) begin
//       if (DRPRDY && ((drpAddr == 9'h150) || ( drpAddr == 9'h14F) || (drpAddr == 9'h03C) || (drpAddr == 9'h03B)))
//          ilaClk <= 1'b1;
//       else 
//          ilaClk <= 1'b0;
//       if (state <= waitGoCmd)
//          ilaClk <= !ilaClk;      
//       if (rstRtl)
//          ilaClk <= 1'b0;
//    end
       
    ila_1 ilaEye (
      .clk(CLK), //ilaClk),                  // input wire clk
      .trig_in(trig),
      .trig_in_ack(),  // output wire trig_in_ack
      .probe0(state),
      .probe1(drpAddr),
      .probe2(samples),            // input wire [30 : 0] probe0
      .probe3(errors),
      .probe4(horz),
      .probe5(vert),
      .probe6(DRPDO),
      .probe7(DRPRDY)
    );
    
    always @ (posedge CLK) if (rstRtl) begin
         state <= waitGoCmd;
         reset_gt_o <= 0;
         eyescanreset_o <= 0;
       end
    else
    case (state)
       
       waitGoCmd: begin
	       trig <= 0;
           drpWe <=0;
           drpEn <= 0;
           drpAddr <= 0;
           drpDi <= 0;
           hsteps <= 0;
	       vsteps <= 0;
           initState <= 0;
           posDn <= 0;
           vs_neg <= 0;
           vs_ut_sign <= 0;
           eyescanreset_o <= eyescanrst_in;
           vert <= 7'h00;//7'b1111110;
           horz <= numSteps_reg >> 1; //16'h0040;
//           horz <= 11'b00000111111;
//           vert <= 11'b00001111111;
           vertOutput <= {vstepSize,4'h0,vs_rangeWire};
           horzOutput <= {PRESCALE,hstepSize[7:0]};
           samplesOutput <= 0;
           errorsOutput <= 0;
           incVert <= 0;
          case(go_reg)
          2: begin
               trig <= 1;               
               state <= setVertOffset;//updateHorizontal;
               numVsteps <= vstepSize == 1 ? maxVertSteps : vstepSize == 2 ? maxVertSteps >> 1 : vstepSize == 4 ? maxVertSteps >> 2 : vstepSize == 8 ? maxVertSteps >> 3 : vstepSize == 16 ? maxVertSteps >> 4 : vstepSize == 32 ? maxVertSteps >> 5 : maxVertSteps >> 1;
             end
          1: 
               //state <= init;
               state <= readHorzReg;
          3: 
               state <= reset;
          0: 
               state <= waitGoCmd;
          endcase
       end
       init: begin
           initState <= 1;
           uDM <= 00;
           drpWe <= 1;
           drpEn <= 0;
           drpDi <= 16'b0000_0011_0000_0100; //control(6b),err_det,eyescan_en,3'b{0},prescale(5b)
           drpAddr <= CONTROL; //9'h03C;
           state <= waitInit;
       end
       waitInit: begin
         if(DRPRDY == 1)
         begin
            drpWe <= 0;
            state <= readHorzReg;
         end
       end       
       readHorzReg: begin
            drpEn <= 1;
            drpWe <= 0;
            drpAddr <= ES_HORZ_OFFSET;
            state <= waitReadHorz;
       end
       waitReadHorz: begin
            drpEn <= 0;
            drpWe <= 0;
            if (DRPRDY == 1) 
            begin
                laneDeskew <= DRPDO[3:0];
                state <= readVertReg;
            end
            else
                state <= waitReadHorz;
       end
       readVertReg: begin
            drpEn <= 1;
            drpWe <= 0;
            drpAddr <= VS_REG;
            state <= waitReadVert;
       end
       waitReadVert: begin
            drpEn <= 0;
            drpWe <= 0;
            if (DRPRDY == 1) 
            begin
                vertMsb <= DRPDO[15:11];
                state <= waitGoCmd;//updateDataMask;
            end
            else
                state <= waitReadVert;
       end
       updateDataMask: begin
           if (uDM == 00) 
           begin
               drpWe <= 1;
               drpEn <= 0;
               drpDi <= 16'hFFFF;
               drpAddr <= DATA_MASK0; //9'h03C;
               uDM <= 01;
           end
           else if (uDM == 01)
           begin
                if (drpWe == 1 & DRPRDY == 1)
                begin
                   drpWe <= 0;           
                   drpAddr <= drpAddr + 1;
                   drpDi <= 16'h0000;
                end 
                else if(drpAddr != 10'h04e) drpWe <= 1;
                if (drpAddr == 10'h04e)//DATA_MASK0+4)
                    uDM <= 10;
           end
           else if (uDM == 10) 
           begin
               drpWe <= 1;
               drpEn <= 0;
               drpDi <= 16'hffff;
               drpAddr <= DATA_MASK5; //9'h03C;
               uDM <= 11;
           end
           else //if (uDM == 11)
           begin
                if (drpWe == 1 & DRPRDY == 1)
                begin
                   drpWe <= 0;           
                   drpAddr <= drpAddr + 1;
                end else if(drpAddr != 10'h0F6) drpWe <= 1;
                if (drpAddr == 10'h0F6)//011110101)//DATA_MASK5+4)
                begin
                    uDM <= 00;                       
                    state <= updateQualMask;
                end
           end
       end
       updateQualMask: begin
           if (uDM == 00) 
           begin
               drpWe <= 1;
               drpEn <= 0;
               drpDi <= 16'hffff;
               drpAddr <= QUAL_MASK0; //9'h03C;
               uDM <= 01;
           end
           else if (uDM == 01)
           begin
                if (drpWe == 1 & DRPRDY == 1)
                begin
                   drpWe <= 0;           
                   drpAddr <= drpAddr + 1;
                end else if(drpAddr != 10'h049) drpWe <= 1;
                if (drpAddr == 10'h049)//QUAL_MASK0+4)
                    uDM <= 10;
           end
           else if (uDM == 10) 
           begin
               drpWe <= 1;
               drpEn <= 0;
               drpDi <= 16'hffff;
               drpAddr <= QUAL_MASK5; //9'h03C;
               uDM <= 11;
           end
           else //if (uDM == 11)
           begin
                if (drpWe == 1 & DRPRDY == 1)
                begin
                   drpWe <= 0;           
                   drpAddr <= drpAddr + 1;
                end else if(drpAddr != 10'h0F1) drpWe <= 1;
                if (drpAddr == 10'h0F1)//QUAL_MASK5+4)
                begin
                    uDM <= 00;
                    state <= setOffsets;
                end
           end
       end
       setOffsets: begin
            horz <= 16'h0000;
            state <= setVertOffset;
            drpWe <= 0;
       end
       setVertOffset: begin
           trig <= 0;   
           drpWe <= 1;
           drpEn <= 0;
           drpDi <= {vertMsb,vs_neg,vs_ut_sign,7'h00,vs_range};//16'h0003;
           drpAddr <= VS_REG; //9'h03C;
           state <= waitVert;
       end
       waitVert: begin
           drpWe <= 0;
           if(DRPRDY) state <= updateHorizontal;//setHorzOffset;
       end
       setHorzOffset: begin
           drpWe <= 1;
           drpEn <= 0;
           drpDi <= {1'b1,horz,laneDeskew};//16'h0000;
           drpAddr <= ES_HORZ_OFFSET; //9'h03C;
           state <= OffsetDn; 
       end
       OffsetDn: begin
           drpWe <= 0;
           if(DRPRDY) state <= run;            
       end
       reset: begin
        // what goes here?
        if(reset_dn_i) 
        begin
            reset_gt_o <= 0;
            eyescanreset_o <= 0;
            state <= waitGoCmd;
        end
        else 
        begin
            reset_gt_o <= 1;
            eyescanreset_o <= 1;
            state <= reset;
        end
       end
       updateHorizontal: begin
	       trig <= 0;
	       cnt <= 0;
           drpEn <=0;
           drpWe <= 1;
           drpAddr <= ES_HORZ_OFFSET;//9'h03C;
	       if (hsteps <= numSteps_reg) //32) begin
	       begin
//	           horz <= horz - hstepSize;
               drpDi <= {1'b1,horz,laneDeskew};
               hsteps <= hsteps + hstepSize;
            end
            else 
            begin
	           drpDi <= {1'b1,2'b0,numSteps_reg >> 1,laneDeskew};//11'h020};//11'b00000111111;
               horz <= numSteps_reg >> 1;//11'h400;//11'b00000111111;
               hsteps <= hstepSize;
               incVert <= 1;
            end
            state <= waitHorizontal;
       end
       waitHorizontal:begin
	       cnt <= cnt+1;
           drpEn <=0;
           drpWe <= 0;
	       if (DRPRDY || cnt[5])
	       begin 
	           horz <= horz - hstepSize;
               if (incVert)//(hsteps >= numSteps_reg)//9'd0)
               begin
                   state <= updateVertical;
                   vert <= vert + vstepSize;
                   incVert <= 0;
               end
	           else
		             state <= run;
           end
           else
               state <= waitHorizontal;
       end
       updateVertical: begin
	        cnt <= 0;
           if (vsteps < numVsteps) 
           begin
                vsteps <= vsteps+1;
                drpWe <= 1;
                drpEn <=0;
                drpAddr <= VS_REG;
//                vert <= vert + vstepSize;
                drpDi <= {vertMsb,vs_neg,vs_ut_sign,(vert),vs_range};//(vert - vstepSize) |16'h2000;
                state <= waitVertical;
           end
           else if (posDn == 0) // Runs again for the negative vertical offset
                begin
                  posDn <= 1;
                  vsteps <= 0;
                  vs_neg <= vs_rangeWire[3];
                  vs_ut_sign <= vs_rangeWire[2];
                  vert <= vstepSize;
                  state <= updateVertical;
                end
                else
                    state <= waitGoCmd;
                
       end
       waitVertical:begin
           drpWe <= 0;
           drpEn <=0;
	       cnt <= cnt+1;
           if (DRPRDY || cnt[5])
                state <= run;
           else
                state <= waitVertical;
       end
       run: begin
	       cnt <= 0;
           state <= waitRun;
           drpWe <= 1;
           drpEn <=0;
           drpAddr <= CONTROL;//9'h03D;  //Set address to ES_CONTROL
           drpDi <= {5'b0,3'h7,3'b0,PRESCALE};//16'b0000_0111_0000_0100;//16'hE301;  // Set run bit and eye Scan enable bit
       end
       waitRun:begin
	       cnt <= cnt+1;
           drpWe <= 0;
           drpEn <=0;
           drpAddr <= 10'b0;  //reset address to ES_CONTROL
//           drpDi <= 16'b0;  // reset data port
           if (DRPRDY  || cnt[5])
              state <= testStart;
           else
              state <= waitRun;
       end
       testStart: begin
           state <= waitStart;
           drpWe <= 0;
           drpEn <= 1;         //Read Status
           cnt<=0;
           drpAddr <= CONTROL_STATUS;//9'h151;  // ES_CONTROL_STATUS
       end
       waitStart: begin
           drpEn <= 0;
           drpWe <=0;
           cnt<= cnt+1;
           if (DRPRDY || cnt[5]) 
           begin
              if (DRPDO[3:1] == 3'b011)//6) //running
                state <=testComplete;
              else
                state <= testStart;
           end
           else
                state <=waitStart;  
       end
       testComplete: begin
           state <= waitComplete;
	        cnt <= 0;
           drpEn <= 1;         //Read Status
           drpWe <= 0;
           drpAddr <= CONTROL_STATUS;//9'h151;  // ES_CONTROL_STATUS
       end
       waitComplete: begin
           drpEn <=0;
	        cnt <= cnt +1;
           if (DRPRDY || cnt[5]) 
           begin
               if ( DRPDO[3:1] == 3'b010 )//|| DRPDO[3:1] == 0)
                  state <= rstCmd;
               else 
                  state <= testComplete;
           end
           else 
              state <=waitComplete;
       end
       rstCmd: begin
    	 drpAddr <= CONTROL;//9'h03D;
	     drpWe <= 1'b1;
         drpEn <= 0;
	     drpDi <= {5'b0,3'h3,3'b0,PRESCALE};//16'b0000_0011_0000_0100;//16'hE300;
         state <= waitRstCmd;
	     cnt <= 0;
       end
       waitRstCmd: begin
	       drpAddr <= 10'b0;
           drpWe <= 1'b0;
           drpEn <= 0;
//           drpDi <= 16'b0;
	        cnt <= cnt+1;
           if (DRPRDY || cnt[5])	   
              //state <= getSamples;
              state <= checkRState;
           else 
              state <=waitRstCmd;	 
       end
       checkRState: begin
           state <= waitRState;
	       cnt <= 0;
           drpEn <= 1;         //Read Status
           drpWe <= 0;
           drpAddr <= CONTROL_STATUS;//9'h151;  // ES_CONTROL_STATUS
       end
       waitRState: begin
           drpEn <=0;
	       cnt <= cnt +1;
           if (DRPRDY) // || cnt[5]) 
           begin 
               if ( DRPDO == 1 )//|| DRPDO[3:1] == 0)
                  state <= getSamples;
               else 
                  state <= checkRState;
           end
           else 
              state <= waitRState;
       end
       getSamples: begin
           drpAddr <= SAMPLE_COUNT;//9'h150;
           drpEn <= 1;
           drpWe <= 0;
           state <= storeSamples;
	       cnt <= 0;
       end
       storeSamples: begin
           drpEn <= 0;
	       cnt <= cnt + 1;
           if (DRPRDY ) //|| cnt[5]) begin
           begin
	          state <= getErrors;
              samples <= DRPDO;
           end
           else 
              state <= storeSamples;
       end
       getErrors: begin
           drpAddr <= ERROR_COUNT;//9'h14F;
           drpEn <= 1;
           state <= storeErrors;
	       cnt <= 0;
       end
       storeErrors: begin
           drpEn <= 0;
//	       cnt <= cnt + 1;
            if (DRPRDY )//|| cnt[5]) begin
            begin
                if  (initState == 1) 
                begin
                    if (DRPDO == 16'hFFFF) 
                    begin
                        state <= realign;
                        raux <= 00;
                    end
                    else if (horz == 0)	
                    begin
                        horz <= rate >> 1;
                        state <= setHorzOffset;
                    end
                    else if (horz == rate >> 1) 
                    begin
                        horz <= -rate << 1;
                        state <= setHorzOffset;
                    end
                    else 
                    begin 
                        state <= waitGoCmd;
                        initState <= 0;
                    end
                end
                else 
                begin
                    state <= updateHorizontal ;
                    trig <= 1;
                    horzOutput <= horz+hstepSize;
                    vertOutput <= {vert,posDn};
                    samplesOutput <= samples;
                    errorsOutput <= DRPDO;
                    errors <= DRPDO;
                end
            end
            else 
            state <= storeErrors;
       end
       realign: begin
            cnt <= cnt + 1;
            if (raux == 00)
            begin
                 drpAddr <= ES_HORZ_OFFSET;//9'h03D;
                 drpWe <= 1'b1;
                 drpEn <= 0;
                 drpDi <= {12'H880,laneDeskew};//16'hE300;
                 raux <= 01;
                 cnt <= 0;
            end 
            else if(raux == 01) 
            begin
                 drpWe <= 1'b0;
                 if(DRPRDY || cnt[5])
                 begin
                     eyescanreset_o <= 1;
                     raux <= 10;//if (DRPRDY) raux <= 10;
                 end
            end
            else if(raux == 10)
            begin
                 drpAddr <= ES_HORZ_OFFSET;//9'h03D;
                 drpWe <= 1'b1;
                 drpEn <= 0;
                 drpDi <= {12'H800,laneDeskew};//16'H800;//16'hE300;
                 raux <= 11;
                 cnt <= 0;
            end
            else  
            begin
                 drpWe <= 1'b0;
                 if (DRPRDY || cnt[5])
                 begin
                    eyescanreset_o <= 0;
                    raux <= 00;
                    state <= setHorzOffset;
                 end
           end
       end
       default:
           state <= waitGoCmd;  // Wait for start command
    endcase

    vio_1 drpCntrlIns (
         .clk(CLK),                // input wire clk
         .probe_in0(DRPDO),    // input wire [15 : 0] probe_in0
         .probe_in1(TXFSMRESETDONE),
         .probe_in2(RXFSMRESETDONE),
         .probe_in3(DRPRDY),
         .probe_in4(state),
         .probe_out0(drpaddrReg),  // output wire [8 : 0] probe_out0
         .probe_out1(drpdiReg[15:0]),  // output wire [14 : 0] probe_out1
         .probe_out2(drpWeCmd),  // output wire [0 : 0] probe_out3
         .probe_out3(drpEnCmd),
         .probe_out4(SOFRST),  // output wire [0 : 0] probe_out4
         .probe_out5(go),
         .probe_out6(rstRtl),
         .probe_out7(hstepSize_v),
         .probe_out8(vstepSize_v),
         .probe_out9(eyescanrst_in),
         .probe_out10(numSteps),
         .probe_out11(gty_select),
         .probe_out12(prescaleWire),
         .probe_out13(vs_rangeWire)
    );

    always @(posedge CLK) begin
      go_reg <= 0;
      v_go <= go;
      if(go != v_go)
        go_reg <= go;// == 1 ? 1 : go == 2 ? 2 : go == 3 ? 4 : 0;
    end
      
    always @(posedge CLK ) begin   // Routine to run the drp using VIO or eyeScan inputs
          
          //numSteps <= rate >> log2;
          numSteps_reg <= numSteps;
          PRESCALE <= prescaleWire;
          vs_range <= vs_rangeWire[1:0];
//          vs_neg <= vs_rangeWire[3];
//          vs_ut_sign <= vs_rangeWire[2];
          hstepSize <= hstepSize_v;
          vstepSize <= vstepSize_v;
          rate_o <= rate_v;
          rate <= 64*rate_v;
          log2 <= hstepSize == 1 ? 0 : hstepSize == 2 ? 1 : hstepSize == 4 ? 2 : hstepSize == 8 ? 3 : hstepSize == 16 ? 4 : hstepSize == 32 ? 5 : hstepSize == 64 ? 6 : hstepSize == 128 ? 7 : hstepSize == 256 ? 8 : hstepSize == 512 ? 9 : hstepSize == 1024 ? 10 : hstepSize == 2*1024 ? 11 :0;// hstepSize == 4*1024 ? 12 : hstepSize == 8*1024 ? 13 : hstepSize == 16*1024 ? 14 : hstepSize == 32*1024 ? 15 : hstepSize == 32*1024 ? 16 : hstepSize == 64*1024 ? 17 : hstepSize == 128*1024 ? 18 : hstepSize == 256*1024 ? 19 : hstepSize == 512*1024 ? 20 : hstepSize == 1024*1024 ? 21 : hstepSize == 2*1024*1024 ? 22 : hstepSize == 4*1024*1024 ? 23 : 0;
          
          if (drpDone) begin
            DRPEN <= 1'b0;
            DRPADDR <= 9'b0;
            DRPDI <= 16'b0;
            DRPWE <= 1'b0;
          end
          if (!drpWeCmd && !drpEnCmd && !drpWe && !drpEn   ) begin
            drpDone <=1'b0;
            DRPEN <= 1'b0;
            DRPADDR <= 9'b0;
            DRPDI <= 16'b0;
            DRPWE <= 1'b0;
          end 
          if ((drpWeCmd || drpWe) && !drpDone) begin
            DRPWE <= 1'b1;
            DRPADDR <= state == waitGoCmd ? drpaddrReg : drpAddr;
            DRPDI <= state == waitGoCmd ? drpdiReg : drpDi;
            DRPEN <= 1'b1;
            drpDone <= 1'b1;
          end
          if ((drpEnCmd || drpEn) && !drpDone) begin
            DRPEN <=1'b1;
            DRPWE <=1'b0;
            DRPADDR <= state == waitGoCmd ? drpaddrReg : drpAddr;
            DRPDI <= state == waitGoCmd ? drpdiReg : drpDi;
            drpDone <= 1'b1; 
          end 
    end
    
//    output_mem_ctrl out_mem(
//        .clk_i(CLK),
//        .samples_i(samples),
//        .errors_i(errors),
//        .horz_i(horz),
//        .ver_i(vert),
//        .valid_i(trig),
//        .reset(SOFRST)    
//    );
    output_mem_ctrl out_mem(
        .clk_i(CLK),
        .samples_i(samplesOutput),
        .errors_i(errorsOutput),
        .horz_i(horzOutput),
        .ver_i(vertOutput),
        .valid_i(trig),
        .reset(SOFRST)    
    );
endmodule
