----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/08/2022 04:21:52 PM
-- Design Name: 
-- Module Name: output_mem_ctrl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity output_mem_ctrl is
    Port ( clk_i : in STD_LOGIC;
           samples_i : in STD_LOGIC_VECTOR (15 downto 0);
           errors_i  : in STD_LOGIC_VECTOR (15 downto 0);
           horz_i    : in STD_LOGIC_VECTOR (15 downto 0);
           ver_i     : in STD_LOGIC_VECTOR (15 downto 0);
           valid_i : in STD_LOGIC;
           reset    : in std_logic);
           
end output_mem_ctrl;

architecture Behavioral of output_mem_ctrl is
    component  design_2_wrapper is
    port (
        clk_in : in STD_LOGIC;
        we_o : out STD_LOGIC_VECTOR ( 3 downto 0 );
        data_i : in STD_LOGIC_VECTOR ( 63 downto 0 );
        addr_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
        data_vio_o : out std_logic_vector(63 downto 0);
        ram_src_slct_o : out STD_LOGIC_VECTOR ( 0 to 0 );
        ram_addr : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ram_din : in STD_LOGIC_VECTOR ( 63 downto 0 );
        ram_dout : out STD_LOGIC_VECTOR ( 63 downto 0 );
        ram_en : in STD_LOGIC;
        ram_rst : in STD_LOGIC;
        ram_we : in STD_LOGIC_VECTOR ( 7 downto 0 );
        reset_in : in STD_LOGIC
    );
    end component;
    signal addr : std_logic_vector(31 downto 0);
    signal data_vio, data, data_test : std_logic_vector(63 downto 0);
    signal we_vio : std_logic_vector(3 downto 0);
    signal wen : std_logic_vector(7 downto 0);
    signal addr_vio : std_logic_vector(15 downto 0);
    signal slct : std_logic;
    signal v_aux : std_logic_vector(1 downto 0) := "00";
    signal v_addr : unsigned(31 downto 0) := x"C0000000";
begin

    design : component design_2_wrapper
    port map(
        clk_in => clk_i,
        reset_in => '0',
        we_o => we_vio,
        data_i => data_vio,
        data_vio_o => data_test,
        addr_o => addr_vio,
        ram_src_slct_o(0) => slct, 
        ram_addr => addr,
        ram_din => data,
        ram_dout => data_vio,
        ram_en => '1',
        ram_rst => '0',
        ram_we => wen
    );
    addr <= std_logic_vector(v_addr) when slct = '1' and v_addr < x"C0010000" else "1100000000000000"&addr_vio;
    process(clk_i)
--    variable v_addr : unsigned(31 downto 0) := x"C0000000";
--    variable v_aux : std_logic_vector(1 downto 0) := "00";
    variable v_horz, v_vert, v_samples, v_errors : std_logic_vector(15 downto 0);
    begin
        if rising_edge(clk_i) then
            if reset = '1' then
                v_addr <= x"C0000000";
                v_aux <= "00";
                wen <= x"00";
            else
                
                wen <= x"FF" when v_aux(1) = '1' and slct = '1' else we_vio&we_vio when slct = '0' else x"00";
                
                if valid_i = '1' then 
                    v_aux <= "01";
                    v_vert := ver_i;
                    v_horz := horz_i;
                    v_samples := samples_i;
                    v_errors := errors_i;
                end if;
                if v_aux = "01" then
                    data <= v_vert & v_horz & v_samples & v_errors;
                    v_aux <= "10";
                elsif v_aux = "10" then
                    v_aux <= "00";
                    v_addr <= v_addr + 8;
                    
--                elsif v_aux = "11" then
--                    v_addr <= v_addr + 4;
--                    data <= samples_i & errors_i;
--                    v_aux <= "00";
                elsif slct = '0' then
                    data <= data_test;
                end if;
             end if;
        end if;
    end process;


end Behavioral;