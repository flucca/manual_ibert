library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

library unisim;
use unisim.vcomponents.all;


entity interlaken_gty is
    generic(
        Lanes        : positive := 4;    -- Number of Lanes (Transmission channels)
        BondNumberOfLanes : positive := 1;
        CARD_TYPE    : integer := 128;
        GTREFCLKS    : integer := 1
    );
    Port ( 
        reset : in std_logic;
        rst_txusr_403M_s : out std_logic;
        rst_rxusr_403M_s : out std_logic;
        
        --------- 125 MHz input, to transceiver (QSFP4 clock)------------
        GTREFCLK_IN_P : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_IN_N : in std_logic_vector(GTREFCLKS-1 downto 0);
--        clk156 : in std_logic;
        -------- 100 MHz input, Free Running CLK (QDR4 clock) -----------
        clk100 : in std_logic;
        ------- VIO signals----------------------------------------------
        rxpmaresetdone_o: out std_logic_vector(3 downto 0);
        txpmaresetdone_o: out std_logic_vector(3 downto 0);
        reset_tx_pll_and_datapath_in : in std_logic;
        reset_tx_datapath_in : in std_logic;
        reset_rx_pll_and_datapath_in : in std_logic;
        reset_rx_datapath_in : in std_logic;
        reset_rx_cdr_stable_out : out std_logic;
        reset_tx_done_out : out std_logic;
        gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        drp_src : in std_logic;
        ------------------- GT data in/out ------------------------------
        TX_Out_P  : out std_logic_vector(Lanes-1 downto 0);
        TX_Out_N  : out std_logic_vector(Lanes-1 downto 0);
        RX_In_P   : in std_logic_vector(Lanes-1 downto 0);
        RX_In_N   : in std_logic_vector(Lanes-1 downto 0);
        
        TX_User_Clock_s : out std_logic_vector(Lanes/4-1 downto 0);
        RX_User_Clock_s : out std_logic_vector(Lanes/4-1 downto 0);
        
        loopback_in       : in std_logic_vector(2 downto 0);
        
        Data_Transceiver_In  : in slv_64_array(0 to Lanes-1);
        Data_Transceiver_Out : out slv_64_array(0 to Lanes-1);
        RX_Datavalid_Out : out std_logic_vector(Lanes-1 downto 0);
        RX_Header_Out_s : out slv_3_array(0 to Lanes-1);
        RX_Headervalid_Out_s : out std_logic_vector(Lanes-1 downto 0);
        TX_Gearboxready_Out : out std_logic_vector(Lanes-1 downto 0);
        
        TX_Header_In : in slv_3_array(0 to Lanes-1)
    );
end interlaken_gty;

architecture Behavioral of interlaken_gty is

    -------------------------- Include Transceiver -----------------------------
--    COMPONENT gtwizard_ultrascale_0
--    PORT (
--        gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
--        gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
--        gtrefclk00_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        qpll0outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        qpll0outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
--        rxgearboxslip_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        txheader_in : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
--        txsequence_in : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
--        txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxdatavalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--        rxheader_out : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
--        rxheadervalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--        rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxstartofseq_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--        txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
--    );
--    END COMPONENT;

    --signal TX_User_Clock, RX_User_Clock : std_logic;
signal TX_User_Clock, RX_User_Clock : std_logic_vector(Lanes/4-1 downto 0);
    
    signal RX_Header_Out : slv_3_array(0 to Lanes-1);
    signal RX_Headervalid_Out : std_logic_vector(Lanes-1 downto 0);
    signal RX_Gearboxslip_In : std_logic_vector(Lanes-1 downto 0);
    signal not_RX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal RX_Resetdone_Out is never read"
    
    signal not_TX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal TX_Resetdone_Out is never read"
    
    signal  gt_txsequence_i         : slv_7_array(0 to Lanes-1);
    signal  gt_txseq_counter_r      : uns_9_array(0 to Lanes-1);

    signal gt_pause_data_valid_r : std_logic_vector(Lanes-1 downto 0);
    signal gt_data_valid_out_i   : std_logic_vector(Lanes-1 downto 0);

    signal GTREFCLK : std_logic_vector((Lanes/4)-1 downto 0);

     ---- GTY added signals -------
     signal txoutclk_out, rxoutclk_out: std_logic_vector(Lanes-1 downto 0);
--     signal tx_active_sync, tx_active_meta : std_logic;
--     signal rx_active_sync, rx_active_meta : std_logic;
     signal gtwiz_userclk_rx_active_in ,gtwiz_userclk_tx_active_in : std_logic_vector((Lanes/4)-1 downto 0);
     signal tx_gearbox_reset, rx_gearbox_reset : std_logic_vector((Lanes/4)-1 downto 0);
     signal rst_txusr_403M, rst_rxusr_403M : std_logic;
     signal gtwiz_reset_rx_done_out : std_logic_vector((Lanes/4)-1 downto 0);
     
     signal GTREFCLK_VERSAL_BUF, GTREFCLK_VERSAL_SEL: std_logic_vector(Lanes/4-1 downto 0);
     
begin
    
--    gtwiz_userclk_tx_active_in <= (others => tx_active_sync);
--    gtwiz_userclk_rx_active_in <= (others => rx_active_sync);
    
    RX_Header_Out_s <= RX_Header_Out;
    RX_Headervalid_Out_s <= RX_Headervalid_Out;
    TX_User_Clock_s <= TX_User_Clock;
    RX_User_Clock_s <= RX_User_Clock;
    rst_txusr_403M_s <= rst_txusr_403M;
    rst_rxusr_403M_s <= rst_rxusr_403M;
    
    INST_RST_TXUSR : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => rst_txusr_403M, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        dest_clk => TX_User_Clock(0),   -- 1-bit input: Destination clock.
        src_arst => reset    -- 1-bit input: Source asynchronous reset signal.
    );
    INST_RST_RXUSR : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => rst_rxusr_403M, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain. 
        dest_clk => RX_User_Clock(0),   -- 1-bit input: Destination clock.
        src_arst => reset    -- 1-bit input: Source asynchronous reset signal.
    );
    
    GTREFCLK_VERSAL_SEL(0) <= GTREFCLK_VERSAL_BUF(0);
    g_2quads: if Lanes/4 > 1 generate
        GTREFCLK_VERSAL_SEL(1) <= GTREFCLK_VERSAL_BUF(1);
    end generate;
    g_3quads: if Lanes/4 > 2 generate
        GTREFCLK_VERSAL_SEL(2) <= GTREFCLK_VERSAL_BUF(2);
    end generate;
    g_4quads: if Lanes/4 > 3 generate
        GTREFCLK_VERSAL_SEL(3) <= GTREFCLK_VERSAL_BUF(3);
    end generate;
    
    g_quads: for quad in 0 to Lanes/4 -1 generate
        signal gtwiz_userdata_tx_in, gtwiz_userdata_rx_out: std_logic_vector(255 downto 0);
        signal gtyrxn_in, gtyrxp_in : std_logic_vector(3 downto 0);
        signal gtytxn_out, gtytxp_out : std_logic_vector(3 downto 0);
        signal rxgearboxslip_in : std_logic_vector(3 downto 0);
        
        signal ch0_txdata_ext_0, ch1_txdata_ext_0, ch2_txdata_ext_0, ch3_txdata_ext_0: std_logic_vector ( 127 downto 0 );
        
        signal txpmaresetdone_out, rxpmaresetdone_out: std_logic_vector(3 downto 0);
        signal gtwiz_reset_all_in : std_logic_vector(0 downto 0);
        signal rxdatavalid_out, rxheadervalid_out : std_logic_vector(7 downto 0);
        signal rxusrclk_in, rxusrclk2_in : std_logic_vector(3 downto 0);
        signal txusrclk_in, txusrclk2_in : std_logic_vector(3 downto 0);
        signal gtwiz_reset_clk_freerun_in : std_logic_vector(0 downto 0);
        --signal gt_rxsequence_o : std_logic_vector(7 downto 0);
        signal txheader_in, rxheader_out : std_logic_vector(23 downto 0);
        signal txsequence_in: std_logic_vector(27 downto 0);
        signal loopback: std_logic_vector(11 downto 0);
        
        signal tx_active_sync, tx_active_meta : std_logic;
        signal rx_active_sync, rx_active_meta : std_logic;
        signal tx_gearbox_reset_i : std_logic;
        signal rx_gearbox_reset_i : std_logic;
        signal not_TX_Resetdone_Out_tx_User_clock, not_RX_Resetdone_Out_rx_User_clock: std_logic;
    begin
        gtwiz_userclk_tx_active_in(quad downto quad) <= (others => tx_active_sync);
        gtwiz_userclk_rx_active_in(quad downto quad) <= (others => rx_active_sync);
        
        gtwiz_userdata_tx_in(63 downto 0) <= Data_Transceiver_In(quad*4+0);    
        gtwiz_userdata_tx_in(127 downto 64) <= Data_Transceiver_In(quad*4+1);    
        gtwiz_userdata_tx_in(191 downto 128) <= Data_Transceiver_In(quad*4+2);    
        gtwiz_userdata_tx_in(255 downto 192) <= Data_Transceiver_In(quad*4+3);    
        
        Data_Transceiver_Out(quad*4 + 0) <= gtwiz_userdata_rx_out(63 downto 0);
        Data_Transceiver_Out(quad*4 + 1) <= gtwiz_userdata_rx_out(127 downto 64);
        Data_Transceiver_Out(quad*4 + 2) <= gtwiz_userdata_rx_out(191 downto 128);
        Data_Transceiver_Out(quad*4 + 3) <= gtwiz_userdata_rx_out(255 downto 192);
        
        gtyrxn_in <= RX_In_N(quad*4+3 downto quad*4);
        gtyrxp_in <= RX_In_P(quad*4+3 downto quad*4);
        TX_Out_N(quad*4+3 downto quad*4) <= gtytxn_out;
        TX_Out_P(quad*4+3 downto quad*4) <= gtytxp_out;
        
        rxgearboxslip_in <= RX_Gearboxslip_In(quad*4+3 downto quad*4);
        txusrclk_in <= (others => TX_User_Clock(quad));
        txusrclk2_in <= txusrclk_in; --Datapath 64b and intw 2; ug578 p105
        rxusrclk_in <= (others => RX_User_Clock(quad));
        rxusrclk2_in <= rxusrclk_in; --Datapath 64b and intw 2; ug578 p105
        
        --TEMPREMOVE
        -- Doesn't work since clk100 is inactive when txpmareset deasserts. So output always stays high
--        TX_Resetdone_Out <= not NOT_TX_Resetdone_Out;
--        RX_Resetdone_Out <= not NOT_RX_Resetdone_Out;
--        sync_RX_Resetdone : xpm_cdc_array_single
--           generic map (
--              DEST_SYNC_FF => 2,
--              INIT_SYNC_FF => 0,
--              SIM_ASSERT_CHK => 0,
--              SRC_INPUT_REG => 0,
--              WIDTH => 4
--           )
--           port map (
--              dest_out => NOT_RX_Resetdone_Out(quad*4+3 downto quad*4),
--              dest_clk => RX_User_Clock(quad),
--              src_clk => clk100,
--              src_in => rxpmaresetdone_out
--           );
        
--        sync_TX_Resetdone : xpm_cdc_array_single
--           generic map (
--              DEST_SYNC_FF => 2,
--              INIT_SYNC_FF => 0,
--              SIM_ASSERT_CHK => 0,
--              SRC_INPUT_REG => 0,
--              WIDTH => 4
--           )
--           port map (
--              dest_out => NOT_TX_Resetdone_Out(quad*4+3 downto quad*4),
--              dest_clk => TX_User_Clock(quad),
--              src_clk => clk100,
--              src_in => txpmaresetdone_out
--           );
           
--        NOT_TX_Resetdone_Out(quad*4+3 downto quad*4) <= not TX_Resetdone_Out(quad*4+3 downto quad*4);
--        NOT_RX_Resetdone_Out(quad*4+3 downto quad*4) <= not RX_Resetdone_Out(quad*4+3 downto quad*4);
        
        not_TX_Resetdone_Out(quad*4+3 downto quad*4) <= not txpmaresetdone_out;
        not_RX_Resetdone_Out(quad*4+3 downto quad*4) <= not rxpmaresetdone_out;
        
        gtwiz_reset_all_in <= (others => reset);
        RX_Datavalid_Out(quad*4+0 ) <= rxdatavalid_out(0);
        RX_Datavalid_Out(quad*4+1 ) <= rxdatavalid_out(2);
        RX_Datavalid_Out(quad*4+2 ) <= rxdatavalid_out(4);
        RX_Datavalid_Out(quad*4+3 ) <= rxdatavalid_out(6);
        
        RX_Headervalid_Out(quad*4+0) <= rxheadervalid_out(0);
        RX_Headervalid_Out(quad*4+1) <= rxheadervalid_out(2);
        RX_Headervalid_Out(quad*4+2) <= rxheadervalid_out(4);
        RX_Headervalid_Out(quad*4+3) <= rxheadervalid_out(6);
        
        gtwiz_reset_clk_freerun_in(0) <= clk100;
    
        txheader_in(5 downto 0)   <= "000" & TX_Header_In(quad*4+0);
        txheader_in(11 downto 6)  <= "000" & TX_Header_In(quad*4+1);
        txheader_in(17 downto 12) <= "000" & TX_Header_In(quad*4+2);
        txheader_in(23 downto 18) <= "000" & TX_Header_In(quad*4+3);
        RX_Header_Out(quad*4+0)   <= rxheader_out(2 downto 0);
        RX_Header_Out(quad*4+1)   <= rxheader_out(8 downto 6);
        RX_Header_Out(quad*4+2)   <= rxheader_out(14 downto 12);
        RX_Header_Out(quad*4+3)   <= rxheader_out(20 downto 18);
        txsequence_in <= gt_txsequence_i(quad*4+3)&
                         gt_txsequence_i(quad*4+2)&
                         gt_txsequence_i(quad*4+1)&
                         gt_txsequence_i(quad*4+0);
                         
        
        loopback <= loopback_in&loopback_in&loopback_in&loopback_in;
                         
        
        
--        g_ultrascale: if CARD_TYPE = 128 generate
--            -------------------------- Include Transceiver -----------------------------
--            COMPONENT gtwizard_ultrascale_0
--              PORT (
--                gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
--                gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
--                gtrefclk00_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                qpll0outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                qpll0outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
--                rxgearboxslip_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                txheader_in : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
--                txsequence_in : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
--                txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--                gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--                gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--                rxdatavalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--                rxheader_out : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
--                rxheadervalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--                rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--                rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--                rxstartofseq_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--                txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--                txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
--              );
--            END COMPONENT;
--        begin
--        gtwizard_ultrascale_0_i : gtwizard_ultrascale_0
--        PORT MAP (
--            loopback_in => loopback,
--            gtyrxn_in => gtyrxn_in,
--            gtyrxp_in => gtyrxp_in,
--            gtytxn_out => gtytxn_out,
--            gtytxp_out => gtytxp_out,
      
--            gtwiz_userclk_tx_active_in => gtwiz_userclk_tx_active_in(quad downto quad),
--            gtwiz_userclk_rx_active_in => gtwiz_userclk_rx_active_in(quad downto quad),
            
--            gtwiz_reset_clk_freerun_in => gtwiz_reset_clk_freerun_in,
--            gtwiz_reset_all_in => gtwiz_reset_all_in,
--            gtwiz_reset_tx_pll_and_datapath_in => "0",
--            gtwiz_reset_tx_datapath_in => "0",
--            gtwiz_reset_rx_pll_and_datapath_in => "0",
--            gtwiz_reset_rx_datapath_in => "0",
--            gtwiz_reset_rx_cdr_stable_out => open,
--            gtwiz_reset_tx_done_out => open,
--            gtwiz_reset_rx_done_out => gtwiz_reset_rx_done_out(quad downto quad),
            
            
--            gtrefclk00_in => GTREFCLK(quad downto quad),
--            qpll0outclk_out => open,
--            qpll0outrefclk_out => open,
--            txusrclk_in => txusrclk_in,
--            txusrclk2_in => txusrclk2_in,
--            rxusrclk_in => rxusrclk_in,
--            rxusrclk2_in => rxusrclk2_in,
            
--            txoutclk_out => txoutclk_out(quad*4+3 downto quad*4),
--            rxoutclk_out => rxoutclk_out(quad*4+3 downto quad*4),
            
--            gtwiz_userdata_tx_in => gtwiz_userdata_tx_in,--Data_Transceiver_In(0),
--            gtwiz_userdata_rx_out => gtwiz_userdata_rx_out, --Data_Transceiver_Out(0),
--            txheader_in => txheader_in,
--            txsequence_in => txsequence_in,
            
            
            
--            gtpowergood_out => open,
            
--            rxdatavalid_out   => rxdatavalid_out,
--            rxheader_out      => rxheader_out,
--            rxheadervalid_out => rxheadervalid_out,
--            rxgearboxslip_in  => rxgearboxslip_in,
            
--            rxpmaresetdone_out => rxpmaresetdone_out,
--            rxstartofseq_out   => open, --gt_rxsequence_o,
            
--            txpmaresetdone_out => txpmaresetdone_out
--        );
--     --end generate;   
--        ------------------------------- Buffering tx/rx out clock signals --------------------------------
--    --g_clockbuffers: for i in 0 to Lanes-1 generate
--        BUFG_GT_TXclk : BUFG_GT
--        port map (
--          O => TX_User_Clock(quad),
--          CE => '1',
--          CEMASK => '0',
--          CLR => not_TX_Resetdone_Out(quad*4), --NOT_TX_Resetdone_Out
--          CLRMASK => '0',
--          DIV => "000",
--          I => txoutclk_out(quad*4)
--        );
        
      
--        BUFG_GT_RXclk : BUFG_GT
--        port map (
--          O => RX_User_Clock(quad), 
--          CE => '1',  
--          CEMASK => '0',
--          CLR => not_RX_Resetdone_Out(quad*4),
--          CLRMASK => '0', 
--          DIV => "000",
--          I => rxoutclk_out(quad*4) 
--        );
--    --end generate;
    
--    -------------------------------- Buffering QSFP GT clock -------------------------------------
--    IBUFDS_GTE4_inst : IBUFDS_GTE4
--    generic map (
--       REFCLK_EN_TX_PATH => '0',  
--       REFCLK_HROW_CK_SEL => "00",
--       REFCLK_ICNTL_RX => "00"    
--    )
--    port map (
--       O => GTREFCLK(quad),       
--       ODIV2 => open, 
--       CEB => '0',    
--       I => GTREFCLK_IN_P(quad),    
--       IB => GTREFCLK_IN_N(quad)    
--    );
--    end generate g_ultrascale;
    
    g_ultrascale_debug: if CARD_TYPE = 129 generate
            -------------------------- Include Transceiver -----------------------------
            COMPONENT gtwizard_ultrascale_0
              PORT (
                gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
                gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
                gtrefclk00_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                qpll0lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                qpll0outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                qpll0outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                drpaddr_in : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
                drpclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                drpdi_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
                drpen_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                drpwe_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                eyescanreset_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
--                rxdfelpmreset_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxgearboxslip_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxlpmen_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--                rxpmareset_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxrate_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
                rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                txdiffctrl_in : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
                txheader_in : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
                txpostcursor_in : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
                txprecursor_in : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
                txsequence_in : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
                txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                drpdo_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
                drprdy_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxdatavalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                rxheader_out : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
                rxheadervalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--                gtrxreset_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxstartofseq_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
              );
            END COMPONENT;
            -- IBERT
            COMPONENT in_system_ibert_0
              PORT (
                drpclk_o : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                gt0_drpen_o : OUT STD_LOGIC;
                gt0_drpwe_o : OUT STD_LOGIC;
                gt0_drpaddr_o : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
                gt0_drpdi_o : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
                gt0_drprdy_i : IN STD_LOGIC;
                gt0_drpdo_i : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                gt1_drpen_o : OUT STD_LOGIC;
                gt1_drpwe_o : OUT STD_LOGIC;
                gt1_drpaddr_o : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
                gt1_drpdi_o : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
                gt1_drprdy_i : IN STD_LOGIC;
                gt1_drpdo_i : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                gt2_drpen_o : OUT STD_LOGIC;
                gt2_drpwe_o : OUT STD_LOGIC;
                gt2_drpaddr_o : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
                gt2_drpdi_o : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
                gt2_drprdy_i : IN STD_LOGIC;
                gt2_drpdo_i : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                gt3_drpen_o : OUT STD_LOGIC;
                gt3_drpwe_o : OUT STD_LOGIC;
                gt3_drpaddr_o : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
                gt3_drpdi_o : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
                gt3_drprdy_i : IN STD_LOGIC;
                gt3_drpdo_i : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                eyescanreset_o : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxrate_o : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
                txdiffctrl_o : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
                txprecursor_o : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
                txpostcursor_o : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
                rxlpmen_o : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxoutclk_i : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                clk : IN STD_LOGIC
              );
            END COMPONENT;
            -- Manual eyeScan
            component eyeScan
            port (
              CLK : in std_logic;
              TXFSMRESETDONE : in std_logic;
              RXFSMRESETDONE : in std_logic;
              reset_gt_o : out std_logic;
              eyescanreset_o : OUT STD_LOGIC;
              rate_o : out std_logic_vector(2 downto 0);
              rxlpmen_o : out std_logic;
              reset_dn_i : in std_logic;
              SOFRST : out std_logic;
              DRPDO : in std_logic_vector(15 downto 0);
              DRPRDY : in std_logic;
              DRPADDR : out std_logic_vector(9 downto 0);
              DRPWE : out std_logic;
              DRPEN : out std_logic;
              DRPDI : out std_logic_vector(15 downto 0);
              gty_select : out std_logic_vector(1 downto 0)
            );
            end component;
            signal drpaddr_in, drpaddr_vio, drpaddr : STD_LOGIC_VECTOR(39 DOWNTO 0) := (others => '0');
            signal drpclk_in : STD_LOGIC_VECTOR(3 DOWNTO 0);
            signal drpdi_in, drpdi_vio, drpdi : STD_LOGIC_VECTOR(63 DOWNTO 0);
            signal drpen_in, drpen_vio,drpen : STD_LOGIC_VECTOR(3 DOWNTO 0);
            signal drpwe_in, drpwe_vio,drpwe : STD_LOGIC_VECTOR(3 DOWNTO 0);
            signal drpdo_out, drpdo_vio, drpdo : STD_LOGIC_VECTOR(63 DOWNTO 0);
            signal drprdy_out, drprdy_vio, drprdy : STD_LOGIC_VECTOR(3 DOWNTO 0);
            signal eyescanreset_in, eyescanreset_ibert, eyescanreset_vio : STD_LOGIC_VECTOR(3 DOWNTO 0);
            signal rxlpmen_in, rxlpmen_ibert, rxlpmen_vio : STD_LOGIC_VECTOR(3 DOWNTO 0);
            signal rxrate_in,rxrate_vio,rxrate_ibert,rxrate_sync : STD_LOGIC_VECTOR(11 DOWNTO 0);
            signal txpostcursor_in : STD_LOGIC_VECTOR(19 DOWNTO 0);
            signal txprecursor_in : STD_LOGIC_VECTOR(19 DOWNTO 0);
            signal txdiffctrl_in : STD_LOGIC_VECTOR(19 DOWNTO 0);
            signal rxdfelpmreset_in : std_logic_vector(3 downto 0);
            signal qpll0lock_out: std_logic;
            signal reset_all_vio : std_logic := '0';
            signal gtrx_reset,pma_reset : std_logic_vector(3 downto 0);
            signal reset_eyescan,reset_dn  : std_logic_vector(3 downto 0);
            signal rxpmareset : std_logic_vector(3 downto 0);
            signal gty_ptr : integer := 0;
            signal gty_select : std_logic_vector(1 downto 0);
            --signal drp_src : std_logic := '0';
        begin
--         xpm_cdc_gray_inst : xpm_cdc_gray
--           generic map (
--              DEST_SYNC_FF => 4,          -- DECIMAL; range: 2-10
--              INIT_SYNC_FF => 0,          -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
--              REG_OUTPUT => 1,            -- DECIMAL; 0=disable registered output, 1=enable registered output
--              SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
--              SIM_LOSSLESS_GRAY_CHK => 0, -- DECIMAL; 0=disable lossless check, 1=enable lossless check
--              WIDTH => 12                  -- DECIMAL; range: 2-32
--           )
--           port map (
--              dest_out_bin => rxrate_sync, -- WIDTH-bit output: Binary input bus (src_in_bin) synchronized to
--                                            -- destination clock domain. This output is combinatorial unless REG_OUTPUT
--                                            -- is set to 1.
        
--              dest_clk => rxoutclk_out(0),         -- 1-bit input: Destination clock.
--              src_clk => drpclk_in(0),           -- 1-bit input: Source clock.
--              src_in_bin => rxrate_vio      -- WIDTH-bit input: Binary input bus that will be synchronized to the
--                                            -- destination clock domain.
        
--           );
              
        gtwizard_ultrascale_0_i : gtwizard_ultrascale_0
        PORT MAP (
            loopback_in => loopback,
            gtyrxn_in => gtyrxn_in,
            gtyrxp_in => gtyrxp_in,
            gtytxn_out => gtytxn_out,
            gtytxp_out => gtytxp_out,
            gtwiz_userclk_tx_active_in => gtwiz_userclk_tx_active_in(quad downto quad),
            gtwiz_userclk_rx_active_in => gtwiz_userclk_rx_active_in(quad downto quad),
            
            gtwiz_reset_clk_freerun_in => gtwiz_reset_clk_freerun_in,
            gtwiz_reset_all_in(0) => gtwiz_reset_all_in(0) ,
            gtwiz_reset_tx_pll_and_datapath_in(0) => reset_tx_pll_and_datapath_in,
            gtwiz_reset_tx_datapath_in(0) => reset_tx_datapath_in,
            gtwiz_reset_rx_pll_and_datapath_in(0) => reset_rx_pll_and_datapath_in,-- or reset_all_vio,
            gtwiz_reset_rx_datapath_in(0) => reset_rx_datapath_in,
            gtwiz_reset_rx_cdr_stable_out(0) => reset_rx_cdr_stable_out,
            gtwiz_reset_tx_done_out(0) => reset_tx_done_out,
            gtwiz_reset_rx_done_out => gtwiz_reset_rx_done_out(quad downto quad),
            
            -- IBERT ports
            drpaddr_in => drpaddr,
            drpclk_in => drpclk_in,
            drpdi_in => drpdi,
            drpen_in => drpen,
            drpwe_in => drpwe,
            drpdo_out => drpdo,
            drprdy_out => drprdy_out,
            eyescanreset_in => eyescanreset_in,
            rxlpmen_in => rxlpmen_in,
--            rxdfelpmreset_in => rxdfelpmreset_in,
            qpll0lock_out(0) => qpll0lock_out,
            rxrate_in => rxrate_in,
            txpostcursor_in => txpostcursor_in,
            txprecursor_in => txprecursor_in,
            txdiffctrl_in => txdiffctrl_in,
            
            gtrefclk00_in => GTREFCLK(quad downto quad),
            qpll0outclk_out => open,
            qpll0outrefclk_out => open,
            txusrclk_in => txusrclk_in,
            txusrclk2_in => txusrclk2_in,
            rxusrclk_in => rxusrclk_in,
            rxusrclk2_in => rxusrclk2_in,
            
            txoutclk_out => txoutclk_out(quad*4+3 downto quad*4),
            rxoutclk_out => rxoutclk_out(quad*4+3 downto quad*4),
            
            gtwiz_userdata_tx_in => gtwiz_userdata_tx_in,--Data_Transceiver_In(0),
            gtwiz_userdata_rx_out => gtwiz_userdata_rx_out, --Data_Transceiver_Out(0),
            txheader_in => txheader_in,
            txsequence_in => txsequence_in,
            --gtrxreset_in => gtrx_reset,
                        
            
            gtpowergood_out => gtpowergood_out,
            
            rxdatavalid_out   => rxdatavalid_out,
            rxheader_out      => rxheader_out,
            rxheadervalid_out => rxheadervalid_out,
            rxgearboxslip_in  => rxgearboxslip_in,
            
            rxpmaresetdone_out => rxpmaresetdone_out,
--            rxpmareset_in => pma_reset,--rxpmareset,
            rxstartofseq_out   => open, --gt_rxsequence_o,
            
            txpmaresetdone_out => txpmaresetdone_out
        );
        
        ibert_0 : in_system_ibert_0
          PORT MAP (
            drpclk_o => drpclk_in,
            gt0_drpen_o => drpen_in(0),
            gt0_drpwe_o => drpwe_in(0),
            gt0_drpaddr_o => drpaddr_in(9 downto 0),
            gt0_drpdi_o => drpdi_in(15 downto 0),
            gt0_drprdy_i => drprdy_out(0),
            gt0_drpdo_i => drpdo_out(15 downto 0),
            gt1_drpen_o => drpen_in(1),
            gt1_drpwe_o => drpwe_in(1),
            gt1_drpaddr_o => drpaddr_in(19 downto 10),
            gt1_drpdi_o => drpdi_in(31 downto 16),
            gt1_drprdy_i => drprdy_out(1),
            gt1_drpdo_i => drpdo_out(31 downto 16),
            gt2_drpen_o => drpen_in(2),
            gt2_drpwe_o => drpwe_in(2),
            gt2_drpaddr_o => drpaddr_in(29 downto 20),
            gt2_drpdi_o => drpdi_in(47 downto 32),
            gt2_drprdy_i => drprdy_out(2),
            gt2_drpdo_i => drpdo_out(47 downto 32),
            gt3_drpen_o => drpen_in(3),
            gt3_drpwe_o => drpwe_in(3),
            gt3_drpaddr_o => drpaddr_in(39 downto 30),
            gt3_drpdi_o => drpdi_in(63 downto 48),
            gt3_drprdy_i => drprdy_out(3),
            gt3_drpdo_i => drpdo_out(63 downto 48),
            eyescanreset_o => eyescanreset_ibert,
            rxrate_o => rxrate_ibert,
            txdiffctrl_o => txdiffctrl_in,
            txprecursor_o => txprecursor_in,
            txpostcursor_o => txpostcursor_in,
            rxlpmen_o => rxlpmen_ibert,
            rxoutclk_i => rxusrclk2_in,
            clk => clk100--txusrclk_in(0)
          );
          
--    process(clk100)
--        variable v_aux : std_logic_vector(1 downto 0);
--    begin
--        if rising_edge(clk100) then
            --gtrx_reset <= x"0";
--            gtwiz_reset_all_in <= (others => '0');
--            pma_reset <= "0000";
--            if reset = '1' then
--                loopback <= (others => '0');
--                v_aux := "01";
--            elsif v_aux = "01" then
--                gtwiz_reset_all_in <= (others => '1');--gtrx_reset <= x"1";
--                v_aux := "10";
--            elsif v_aux = "10" then
--                loopback <= loopback_in&loopback_in&loopback_in&loopback_in;
--                pma_reset <= "1111";
--                v_aux := "00";
--            end if;
--        end if;
--    end process;
                   
--          rxrate_in(2+3*gty_ptr downto gty_ptr*3) <= rxrate_ibert(2+3*gty_ptr downto gty_ptr*3) when drp_src = '0' else rxrate_vio(2+3*gty_ptr downto gty_ptr*3); --rxrate_vio(2+3*i downto i*3)
--          eyescanreset_in(gty_ptr) <= eyescanreset_ibert(gty_ptr) when drp_src = '0' else eyescanreset_vio(gty_ptr);
--          rxlpmen_in(gty_ptr) <= rxlpmen_ibert(gty_ptr) when drp_src = '0' else rxlpmen_vio(gty_ptr);
--          drpen(gty_ptr) <= drpen_in(gty_ptr) when drp_src = '0' else drpen_vio(gty_ptr);
--          drpwe(gty_ptr) <= drpwe_in(gty_ptr) when drp_src = '0' else drpwe_vio(gty_ptr);
--          drpaddr((9+(10*gty_ptr)) downto (10*gty_ptr)) <= drpaddr_in((9+(10*gty_ptr)) downto (10*gty_ptr)) when drp_src = '0' else drpaddr_vio((9+(10*gty_ptr)) downto (10*gty_ptr));
--          drpdi(15+16*gty_ptr downto 16*gty_ptr) <= drpdi_in(15+16*gty_ptr downto 16*gty_ptr) when drp_src = '0' else drpdi_vio(15+16*gty_ptr downto 16*gty_ptr);

--          drpdo_vio(15+16*gty_ptr downto 16*gty_ptr) <= drpdo(15+16*gty_ptr downto 16*gty_ptr);
--          drpdo_out(15+16*gty_ptr downto 16*gty_ptr) <= drpdo(15+16*gty_ptr downto 16*gty_ptr);
          rxrate_in <= rxrate_ibert;-- when drp_src = '0' else rxrate_vio; --rxrate_vio(2+3*i downto i*3)
          eyescanreset_in <= eyescanreset_ibert when drp_src = '0' else eyescanreset_vio;
          rxlpmen_in <= rxlpmen_ibert when drp_src = '0' else rxlpmen_vio;
          drpen <= drpen_in when drp_src = '0' else drpen_vio;
          drpwe <= drpwe_in when drp_src = '0' else drpwe_vio;
          drpaddr <= drpaddr_in when drp_src = '0' else drpaddr_vio;
          drpdi <= drpdi_in when drp_src = '0' else drpdi_vio;

          drpdo_vio <= drpdo;
          drpdo_out <= drpdo;
          
          man_ibert : eyeScan
          port map( 
            CLK => drpclk_in(0),  --- IBERT drp clock ?? otherwise I need to change the clocking scheme for the drp ports in the gty
            TXFSMRESETDONE => txpmaresetdone_out(gty_ptr), -- ??
            RXFSMRESETDONE => rxpmaresetdone_out(gty_ptr),
            eyescanreset_o => eyescanreset_vio(gty_ptr),
            reset_gt_o => reset_eyescan(gty_ptr),
            rate_o => rxrate_vio(2+3*gty_ptr downto gty_ptr*3),
            reset_dn_i => reset_dn(gty_ptr),
            rxlpmen_o => rxlpmen_vio(gty_ptr),
            SOFRST => open,
            DRPDO => drpdo_vio(15+16*gty_ptr downto 16*gty_ptr),
            DRPRDY => drprdy_out(gty_ptr),
            DRPADDR => drpaddr_vio((9+(10*gty_ptr)) downto (10*gty_ptr)),
            DRPWE => drpwe_vio(gty_ptr),
            DRPEN => drpen_vio(gty_ptr),
            DRPDI => drpdi_vio(15+16*gty_ptr downto 16*gty_ptr),
            gty_select => gty_select
          );
          gty_ptr <= 0;--to_integer(unsigned(gty_select));
          
        ------------------------------- Buffering tx/rx out clock signals --------------------------------
    --g_clockbuffers: for i in 0 to Lanes-1 generate
        BUFG_GT_TXclk : BUFG_GT
        port map (
          O => TX_User_Clock(quad),
          CE => '1',
          CEMASK => '0',
          CLR => not_TX_Resetdone_Out(quad*4), --NOT_TX_Resetdone_Out
          CLRMASK => '0',
          DIV => "000",
          I => txoutclk_out(quad*4)
        );
        
      
        BUFG_GT_RXclk : BUFG_GT
        port map (
          O => RX_User_Clock(quad), 
          CE => '1',  
          CEMASK => '0',
          CLR => not_RX_Resetdone_Out(quad*4),
          CLRMASK => '0', 
          DIV => "000",
          I => rxoutclk_out(quad*4) 
        );
    --end generate;
    
    -------------------------------- Buffering QSFP GT clock -------------------------------------
    IBUFDS_GTE4_inst : IBUFDS_GTE4
    generic map (
       REFCLK_EN_TX_PATH => '0',  
       REFCLK_HROW_CK_SEL => "00",
       REFCLK_ICNTL_RX => "00"    
    )
    port map (
       O => GTREFCLK(quad),       
       ODIV2 => open, 
       CEB => '0',    
       I => GTREFCLK_IN_P(quad),    
       IB => GTREFCLK_IN_N(quad)    
    );
    end generate g_ultrascale_debug;
    
        g_versalprime: if CARD_TYPE = 180 or CARD_TYPE = 181 generate
            component transceiver_versal_interlaken_wrapper is
              port (
                GT_REFCLK0 : in STD_LOGIC;
                GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
                apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
                apb3clk_quad : in STD_LOGIC;
                ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch0_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch0_rxcdrlock_ext_0 : out STD_LOGIC;
                ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch0_rxpolarity_ext_0 : in STD_LOGIC;
                ch0_rxresetdone_ext_0 : out STD_LOGIC;
                ch0_rxslide_ext_0 : in STD_LOGIC;
                ch0_rxvalid_ext_0 : out STD_LOGIC;
                ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_txpolarity_ext_0 : in STD_LOGIC;
                ch0_txresetdone_ext_0 : out STD_LOGIC;
                ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch1_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch1_rxcdrlock_ext_0 : out STD_LOGIC;
                ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch1_rxpolarity_ext_0 : in STD_LOGIC;
                ch1_rxresetdone_ext_0 : out STD_LOGIC;
                ch1_rxslide_ext_0 : in STD_LOGIC;
                ch1_rxvalid_ext_0 : out STD_LOGIC;
                ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_txpolarity_ext_0 : in STD_LOGIC;
                ch1_txresetdone_ext_0 : out STD_LOGIC;
                ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch2_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch2_rxcdrlock_ext_0 : out STD_LOGIC;
                ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch2_rxpolarity_ext_0 : in STD_LOGIC;
                ch2_rxresetdone_ext_0 : out STD_LOGIC;
                ch2_rxslide_ext_0 : in STD_LOGIC;
                ch2_rxvalid_ext_0 : out STD_LOGIC;
                ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_txpolarity_ext_0 : in STD_LOGIC;
                ch2_txresetdone_ext_0 : out STD_LOGIC;
                ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch3_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch3_rxcdrlock_ext_0 : out STD_LOGIC;
                ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch3_rxpolarity_ext_0 : in STD_LOGIC;
                ch3_rxresetdone_ext_0 : out STD_LOGIC;
                ch3_rxslide_ext_0 : in STD_LOGIC;
                ch3_rxvalid_ext_0 : out STD_LOGIC;
                ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_txpolarity_ext_0 : in STD_LOGIC;
                ch3_txresetdone_ext_0 : out STD_LOGIC;
                ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
                lcpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
                link_status_gt_bridge_ip_0 : out STD_LOGIC;
                rate_sel_gt_bridge_ip_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
                reset_rx_datapath_in_0 : in STD_LOGIC;
                reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
                reset_tx_datapath_in_0 : in STD_LOGIC;
                reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
                rpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
                rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                rxusrclk_gt_bridge_ip_0 : out STD_LOGIC;
                tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                txusrclk_gt_bridge_ip_0 : out STD_LOGIC
              );
            end component transceiver_versal_interlaken_wrapper;
            signal ch0_rxdata_ext_0: std_logic_vector(127 downto 0);
            signal ch1_rxdata_ext_0: std_logic_vector(127 downto 0);
            signal ch2_rxdata_ext_0: std_logic_vector(127 downto 0);
            signal ch3_rxdata_ext_0: std_logic_vector(127 downto 0);
        begin
        IBUFDS_GTE5_inst : IBUFDS_GTE5
            generic map (
               REFCLK_EN_TX_PATH => '0',  
               REFCLK_HROW_CK_SEL => 0,
               REFCLK_ICNTL_RX => 0    
            )
            port map (
               O => GTREFCLK_VERSAL_BUF(quad),       
               ODIV2 => open, 
               CEB => '0',    
               I => GTREFCLK_IN_P(quad),    
               IB => GTREFCLK_IN_N(quad)    
            );
            
        
        gtwiz_userdata_rx_out(63 downto 0)    <= ch0_rxdata_ext_0(63 downto 0);
        gtwiz_userdata_rx_out(127 downto 64)  <= ch1_rxdata_ext_0(63 downto 0);
        gtwiz_userdata_rx_out(191 downto 128) <= ch2_rxdata_ext_0(63 downto 0);
        gtwiz_userdata_rx_out(255 downto 192) <= ch3_rxdata_ext_0(63 downto 0);
        
        ch0_txdata_ext_0 <= x"0000_0000_0000_0000" & gtwiz_userdata_tx_in(63 downto 0);
        ch1_txdata_ext_0 <= x"0000_0000_0000_0000"&gtwiz_userdata_tx_in(127 downto 64);
        ch2_txdata_ext_0 <= x"0000_0000_0000_0000"&gtwiz_userdata_tx_in(191 downto 128);
        ch3_txdata_ext_0 <= x"0000_0000_0000_0000"&gtwiz_userdata_tx_in(255 downto 192);
        
        quad0: transceiver_versal_interlaken_wrapper 
              port map(
                GT_REFCLK0                       => GTREFCLK_VERSAL_SEL(quad),
                GT_Serial_grx_n                  => gtyrxn_in,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_grx_p                  => gtyrxp_in,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_n                  => gtytxn_out,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_p                  => gtytxp_out,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                apb3clk_gt_bridge_ip_0           => clk100,--: in STD_LOGIC;
                apb3clk_quad                     => clk100,--: in STD_LOGIC;
                ch0_loopback_0                   => loopback(2 downto 0),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch0_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch0_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch0_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxdata_ext_0                 => ch0_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_rxdatavalid_ext_0            => rxdatavalid_out(1 downto 0),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxgearboxslip_ext_0          => rxgearboxslip_in(0),--: in STD_LOGIC;
                ch0_rxheader_ext_0               => rxheader_out(5 downto 0),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_rxheadervalid_ext_0          => rxheadervalid_out(1 downto 0),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxpmaresetdone_ext_0         => rxpmaresetdone_out(0),--: out STD_LOGIC;
                ch0_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch0_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch0_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch0_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch0_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_txdata_ext_0                 => ch0_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_txheader_ext_0               => txheader_in(5 downto 0),--: in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch0_txresetdone_ext_0            => txpmaresetdone_out(0),--: out STD_LOGIC;
                ch0_txsequence_ext_0             => txsequence_in(6 downto 0),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch1_loopback_0                   => loopback(5 downto 3),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch1_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch1_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch1_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxdata_ext_0                 => ch1_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_rxdatavalid_ext_0            => rxdatavalid_out(3 downto 2),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxgearboxslip_ext_0          => rxgearboxslip_in(1),--: in STD_LOGIC;
                ch1_rxheader_ext_0               => rxheader_out(11 downto 6),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_rxheadervalid_ext_0          => rxheadervalid_out(3 downto 2),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxpmaresetdone_ext_0         => rxpmaresetdone_out(1),--: out STD_LOGIC;
                ch1_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch1_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch1_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch1_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch1_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_txdata_ext_0                 => ch1_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_txheader_ext_0               => txheader_in(11 downto 6), -- in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch1_txresetdone_ext_0            => txpmaresetdone_out(1),--: out STD_LOGIC;
                ch1_txsequence_ext_0             => txsequence_in(13 downto 7),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch2_loopback_0                   => loopback(8 downto 6),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch2_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch2_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch2_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxdata_ext_0                 => ch2_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_rxdatavalid_ext_0            => rxdatavalid_out(5 downto 4),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxgearboxslip_ext_0          => rxgearboxslip_in(2),--: in STD_LOGIC;
                ch2_rxheader_ext_0               => rxheader_out(17 downto 12),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_rxheadervalid_ext_0          => rxheadervalid_out(5 downto 4),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxpmaresetdone_ext_0         => rxpmaresetdone_out(2),--: out STD_LOGIC;
                ch2_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch2_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch2_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch2_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch2_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_txdata_ext_0                 => ch2_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_txheader_ext_0               => txheader_in(17 downto 12), -- in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch2_txresetdone_ext_0            => txpmaresetdone_out(2),--: out STD_LOGIC;
                ch2_txsequence_ext_0             => txsequence_in(20 downto 14),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch3_loopback_0                   => loopback(11 downto 9),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch3_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch3_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch3_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxdata_ext_0                 => ch3_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_rxdatavalid_ext_0            => rxdatavalid_out(7 downto 6),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxgearboxslip_ext_0          => rxgearboxslip_in(3),--: in STD_LOGIC;
                ch3_rxheader_ext_0               => rxheader_out(23 downto 18),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_rxheadervalid_ext_0          => rxheadervalid_out(7 downto 6),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxpmaresetdone_ext_0         => rxpmaresetdone_out(3),--: out STD_LOGIC;
                ch3_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch3_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch3_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch3_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch3_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_txdata_ext_0                 => ch3_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_txheader_ext_0               => txheader_in(23 downto 18), -- in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch3_txresetdone_ext_0            => txpmaresetdone_out(3),--: out STD_LOGIC;
                ch3_txsequence_ext_0             => txsequence_in(27 downto 21),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                gt_reset_gt_bridge_ip_0          => gtwiz_reset_all_in(0), --: in STD_LOGIC;
                lcpll_lock_gt_bridge_ip_0        => open,--: out STD_LOGIC;
                link_status_gt_bridge_ip_0       => open,--: out STD_LOGIC;
                rate_sel_gt_bridge_ip_0          => "0000",--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                reset_rx_datapath_in_0           => '0',--: in STD_LOGIC;
                reset_rx_pll_and_datapath_in_0   => '0',--: in STD_LOGIC;
                reset_tx_datapath_in_0           => '0',--: in STD_LOGIC;
                reset_tx_pll_and_datapath_in_0   => '0',--: in STD_LOGIC;
                rpll_lock_gt_bridge_ip_0         => open,--: out STD_LOGIC;
                rx_resetdone_out_gt_bridge_ip_0  => gtwiz_reset_rx_done_out(quad),--: out STD_LOGIC;
                rxusrclk_gt_bridge_ip_0          => rx_user_clock_s(quad),--: out STD_LOGIC;
                tx_resetdone_out_gt_bridge_ip_0  => open,--: out STD_LOGIC;
                txusrclk_gt_bridge_ip_0          => tx_user_clock_s(quad) --: out STD_LOGIC
              );
    end generate g_versalprime;
      xpm_cdc_sync_rst_inst_not_tx_resetdone_out : xpm_cdc_sync_rst
    generic map (
       DEST_SYNC_FF => 2,
       INIT => 1,
       INIT_SYNC_FF => 0,
       SIM_ASSERT_CHK => 0
    )
    port map (
       dest_rst => not_TX_Resetdone_Out_tx_User_clock,
       dest_clk => TX_User_Clock(quad),
       src_rst => not_TX_Resetdone_Out(quad)
    );
    xpm_cdc_sync_rst_inst_not_rx_resetdone_out : xpm_cdc_sync_rst
    generic map (
       DEST_SYNC_FF => 2,
       INIT => 1,
       INIT_SYNC_FF => 0,
       SIM_ASSERT_CHK => 0
    )
    port map (
       dest_rst => not_RX_Resetdone_Out_rx_User_clock,
       dest_clk => RX_User_Clock(quad),
       src_rst => not_RX_Resetdone_Out(quad)
    );
    ------------------------------ Set GTY active signals ---------------------------------------
    --! FS: TX_Resetdone_Out and RX_Resetdone_out prepended with not_ because they are inverted
    --! FS: Syncronized the signals with xpm_cdc_sync_rst to RX/TX user_clock to avoid timing violations 
    tx_active : process (TX_User_Clock, not_TX_Resetdone_Out_tx_User_clock) 
    begin
        if not_TX_Resetdone_Out_tx_User_clock = '1' then
                tx_active_meta <= '0';
                tx_active_sync <= '0';
        elsif rising_edge(TX_User_Clock(quad)) then
                tx_active_meta <= '1';
                tx_active_sync <= tx_active_meta;
        end if;
    end process;
    
    rx_active : process (RX_User_Clock, not_RX_Resetdone_Out_rx_User_clock) 
    begin
        if not_RX_Resetdone_Out_rx_User_clock = '1' then
                rx_active_meta <= '0';
                rx_active_sync <= '0';
        elsif rising_edge(RX_User_Clock(quad)) then
            
            
                rx_active_meta <= '1';
                rx_active_sync <= rx_active_meta;
            
        end if;
    end process;
    
    ------------------------------- Gearbox reset -------------------------------------
--    tx_gearbox_reset <= rst_txusr_403M or not gtwiz_userclk_tx_active_in(0); -- tx_gearbox_reset <= reset or not gtwiz_userclk_tx_active_in(0);
--    rx_gearbox_reset <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(0);
--    tx_gearbox_reset_i <= reset or not gtwiz_userclk_tx_active_in(quad);
--    rx_gearbox_reset_i <= reset or not gtwiz_reset_rx_done_out(quad);

--    tx_gearbox_reset(quad) <= rst_txusr_403M or not gtwiz_userclk_tx_active_in(quad);
--    rx_gearbox_reset(quad) <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(quad);
    tx_gearbox_reset_i <= rst_txusr_403M or not gtwiz_userclk_tx_active_in(quad);
    rx_gearbox_reset_i <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(quad);
    
    tx_gearbox_reset_sync_inst : xpm_cdc_single
           generic map (
              DEST_SYNC_FF => 2,   
              INIT_SYNC_FF => 0,   
              SIM_ASSERT_CHK => 0, 
              SRC_INPUT_REG => 0 
           )                       
           port map (              
              dest_out => tx_gearbox_reset(quad),
              dest_clk => TX_User_Clock(quad),
              src_clk => clk100,  
              src_in => tx_gearbox_reset_i     
           );
        
        rx_gearbox_reset_sync_inst : xpm_cdc_single
           generic map (
              DEST_SYNC_FF => 2,   
              INIT_SYNC_FF => 0,   
              SIM_ASSERT_CHK => 0, 
              SRC_INPUT_REG => 0 
           )                       
           port map (              
              dest_out => rx_gearbox_reset(quad),
              dest_clk => RX_User_Clock(quad),
              src_clk => clk100,  
              src_in => rx_gearbox_reset_i     
           );
        
    end generate g_quads;
    
    ------------------------------- Gearbox logic -------------------------------------
    g_gearbox: for i in 0 to Lanes-1 generate
        gt_data_valid_out_i(i) <=  '1' when ((gt_txsequence_i(i) /= "0010101") and (gt_txsequence_i(i) /= "0101011") and (gt_txsequence_i(i) /= "1000001")) else
                                        '0';
                                        
        process(TX_User_Clock)
        begin
            if rising_edge (TX_User_Clock(i/4)) then
                gt_pause_data_valid_r(i) <=  gt_data_valid_out_i(i) ;
            end if;
        end process;

        TX_Gearboxready_Out(i)  <= '1' when (gt_pause_data_valid_r(i)='1') else '0';

        ------------------------------- TX Gearbox sequencer -------------------------------------  
        process(TX_User_Clock)
        begin
            if rising_edge (TX_User_Clock(i/4)) then
                if((tx_gearbox_reset(i/4)='1') or (gt_txseq_counter_r(i) = 133)) then--66)) then
                    gt_txseq_counter_r(i) <=  (others => '0') ;
                else
                    gt_txseq_counter_r(i) <=  gt_txseq_counter_r(i) + 1 ;
                end if;
            end if;
        end process;
        gt_txsequence_i(i)         <= std_logic_vector(gt_txseq_counter_r(i)(7 downto 1)); --(6 downto 0));
        
        --rx_gearbox_reset <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(0); --rx_gearbox_reset <= reset or not gtwiz_reset_rx_done_out(0);
        
        ------------------------------- RX Gearbox bitslip -- -------------------------------------
        block_sync_sm_0_i  :  entity work.Transceiver_10g_64b67b_BLOCK_SYNC_SM
            generic map
            (
                SH_CNT_MAX          => 64,
                SH_INVALID_CNT_MAX  => 16
                --Lanes               => Lanes
            )
            port map
            (
                -- User Interface
                BLOCKSYNC_OUT             =>    open,
                RXGEARBOXSLIP_OUT         =>    RX_Gearboxslip_In(i),
                RXHEADER_IN               =>    RX_Header_Out(i),
                RXHEADERVALID_IN          =>    RX_Headervalid_Out(i),
 
                -- System Interface
                USER_CLK                  =>    RX_User_Clock(i/4),
                SYSTEM_RESET              =>    rx_gearbox_reset(i/4)
            );
    end generate;
end Behavioral;
