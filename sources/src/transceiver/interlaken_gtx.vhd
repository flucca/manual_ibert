library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

library unisim;
use unisim.vcomponents.all;


entity interlaken_gtx is
    generic(
        Lanes        : positive := 4;    -- Number of Lanes (Transmission channels)
        BondNumberOfLanes : positive := 1;
        CARD_TYPE    : integer := 128;
        GTREFCLKS    : integer := 1
    );
    Port ( 
        reset : in std_logic;
        rst_txusr_403M_s : out std_logic;
        rst_rxusr_403M_s : out std_logic;
        
        --------- 125 MHz input, to transceiver (QSFP4 clock)------------
        GTREFCLK_IN_P : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_IN_N : in std_logic_vector(GTREFCLKS-1 downto 0);
        
        -------- 100 MHz input, Free Running CLK (QDR4 clock) -----------
        clk100 : in std_logic;

        ------------------- GT data in/out ------------------------------
        TX_Out_P  : out std_logic_vector(Lanes-1 downto 0);
        TX_Out_N  : out std_logic_vector(Lanes-1 downto 0);
        RX_In_P   : in std_logic_vector(Lanes-1 downto 0);
        RX_In_N   : in std_logic_vector(Lanes-1 downto 0);
        
        TX_User_Clock_s : out std_logic_vector(Lanes/4-1 downto 0);
        RX_User_Clock_s : out std_logic_vector(Lanes/4-1 downto 0);
        
        loopback_in       : in std_logic_vector(2 downto 0);
        
        Data_Transceiver_In  : in slv_64_array(0 to Lanes-1);
        Data_Transceiver_Out : out slv_64_array(0 to Lanes-1);
        RX_Datavalid_Out : out std_logic_vector(Lanes-1 downto 0);
        RX_Header_Out_s : out slv_3_array(0 to Lanes-1);
        RX_Headervalid_Out_s : out std_logic_vector(Lanes-1 downto 0);
        TX_Gearboxready_Out : out std_logic_vector(Lanes-1 downto 0);
        
        TX_Header_In : in slv_3_array(0 to Lanes-1)
    );
end interlaken_gtx;

architecture Behavioral of interlaken_gtx is

    signal TX_User_Clock, RX_User_Clock : std_logic_vector(Lanes/4-1 downto 0);
    
    signal RX_Header_Out : slv_3_array(0 to Lanes-1);
    signal RX_Headervalid_Out : std_logic_vector(Lanes-1 downto 0);
    signal RX_Gearboxslip_In : std_logic_vector(Lanes-1 downto 0);
    
    signal txoutclk_out, rxoutclk_out: std_logic_vector(Lanes-1 downto 0);
    signal tx_gearbox_reset, rx_gearbox_reset : std_logic_vector((Lanes/4)-1 downto 0);
    signal rst_txusr_403M, rst_rxusr_403M : std_logic;
     
begin

    RX_Header_Out_s <= RX_Header_Out;
    RX_Headervalid_Out_s <= RX_Headervalid_Out;
    TX_User_Clock_s <= TX_User_Clock;
    RX_User_Clock_s <= RX_User_Clock;
    rst_txusr_403M_s <= rst_txusr_403M;
    rst_rxusr_403M_s <= rst_rxusr_403M;
    
    INST_RST_TXUSR : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => rst_txusr_403M, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        dest_clk => TX_User_Clock(0),   -- 1-bit input: Destination clock.
        src_arst => reset    -- 1-bit input: Source asynchronous reset signal.
    );
    INST_RST_RXUSR : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => rst_rxusr_403M, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain. 
        dest_clk => RX_User_Clock(0),   -- 1-bit input: Destination clock.
        src_arst => reset    -- 1-bit input: Source asynchronous reset signal.
    );
    
    g_quads: for quad in 0 to Lanes/4 -1 generate
        
    begin
        
        g_virtex7: if CARD_TYPE = 707 generate
            -------------------------- Include Transceiver -----------------------------
            component Transceiver_10g_64b67b
            Port ( 
                SOFT_RESET_TX_IN : in STD_LOGIC;
                SOFT_RESET_RX_IN : in STD_LOGIC;
                DONT_RESET_ON_DATA_ERROR_IN : in STD_LOGIC;
                Q0_CLK1_GTREFCLK_PAD_N_IN : in STD_LOGIC;
                Q0_CLK1_GTREFCLK_PAD_P_IN : in STD_LOGIC;
                GT0_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;
                GT0_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;
                GT0_DATA_VALID_IN : in STD_LOGIC;
                GT0_TX_MMCM_LOCK_OUT : out STD_LOGIC;
                GT0_RX_MMCM_LOCK_OUT : out STD_LOGIC;
                GT0_TXUSRCLK_OUT : out STD_LOGIC;
                GT0_TXUSRCLK2_OUT : out STD_LOGIC;
                GT0_RXUSRCLK_OUT : out STD_LOGIC;
                GT0_RXUSRCLK2_OUT : out STD_LOGIC;
                gt0_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
                gt0_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
                gt0_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
                gt0_drpen_in : in STD_LOGIC;
                gt0_drprdy_out : out STD_LOGIC;
                gt0_drpwe_in : in STD_LOGIC;
                gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 7 downto 0 );
                gt0_eyescanreset_in : in STD_LOGIC;
                gt0_rxuserrdy_in : in STD_LOGIC;
                gt0_eyescandataerror_out : out STD_LOGIC;
                gt0_eyescantrigger_in : in STD_LOGIC;
                gt0_rxdata_out : out STD_LOGIC_VECTOR ( 63 downto 0 );
                gt0_gtxrxp_in : in STD_LOGIC;
                gt0_gtxrxn_in : in STD_LOGIC;
                gt0_rxdfelpmreset_in : in STD_LOGIC;
                gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );
                gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
                gt0_rxoutclkfabric_out : out STD_LOGIC;
                gt0_rxdatavalid_out : out STD_LOGIC;
                gt0_rxheader_out : out STD_LOGIC_VECTOR ( 2 downto 0 );
                gt0_rxheadervalid_out : out STD_LOGIC;
                gt0_rxgearboxslip_in : in STD_LOGIC;
                gt0_gtrxreset_in : in STD_LOGIC;
                gt0_rxpmareset_in : in STD_LOGIC;
                gt0_rxresetdone_out : out STD_LOGIC;
                gt0_gttxreset_in : in STD_LOGIC;
                gt0_txuserrdy_in : in STD_LOGIC;
                gt0_txdata_in : in STD_LOGIC_VECTOR ( 63 downto 0 );
                gt0_gtxtxn_out : out STD_LOGIC;
                gt0_gtxtxp_out : out STD_LOGIC;
                gt0_txoutclkfabric_out : out STD_LOGIC;
                gt0_txoutclkpcs_out : out STD_LOGIC;
                gt0_txgearboxready_out : out STD_LOGIC;
                gt0_txheader_in : in STD_LOGIC_VECTOR ( 2 downto 0 );
                gt0_txstartseq_in : in STD_LOGIC;
                gt0_txresetdone_out : out STD_LOGIC;
                GT0_QPLLLOCK_OUT : out STD_LOGIC;
                GT0_QPLLREFCLKLOST_OUT : out STD_LOGIC;
                GT0_QPLLOUTCLK_OUT : out STD_LOGIC;
                GT0_QPLLOUTREFCLK_OUT : out STD_LOGIC;
                sysclk_in : in STD_LOGIC
            );
        
            end component;
            
            signal GT0_DATA_VALID_IN :  std_logic_vector(3 downto 0);
            signal GT0_TX_FSM_RESET_DONE_OUT : std_logic_vector(3 downto 0); --Todo use as status bit -- @suppress "signal GT_TX_FSM_RESET_DONE_OUT is never read"
            signal RX_Resetdone_Out : std_logic_vector(3 downto 0); --Todo use as status bit -- @suppress "signal RX_Resetdone_Out is never read"
            signal TX_Startseq_In : std_logic;
            signal TX_Resetdone_Out : std_logic_vector(3 downto 0); --Todo use as status bit -- @suppress "signal TX_Resetdone_Out is never read"
            signal TX_Gearboxready,TX_Gearboxready_p1 : std_logic_vector(Lanes-1 downto 0);
    
            begin
            
            GT0_DATA_VALID_IN <= (others => '1');
           
            ------------------------------- Transceiver --------------------------------
            Transceiver_10g_64b67b_i : Transceiver_10g_64b67b
            port map (
                SOFT_RESET_TX_IN            => reset,
                SOFT_RESET_RX_IN            => reset,
                DONT_RESET_ON_DATA_ERROR_IN => '0',
                Q0_CLK1_GTREFCLK_PAD_N_IN   => GTREFCLK_IN_N(quad),
                Q0_CLK1_GTREFCLK_PAD_P_IN   => GTREFCLK_IN_P(quad),
         
                GT0_TX_FSM_RESET_DONE_OUT => GT0_TX_FSM_RESET_DONE_OUT(0),
                GT0_RX_FSM_RESET_DONE_OUT => open,
                GT0_DATA_VALID_IN         => GT0_DATA_VALID_IN(0),
                GT0_TX_MMCM_LOCK_OUT      => open,
                GT0_RX_MMCM_LOCK_OUT      => open,
         
                GT0_TXUSRCLK_OUT    => open,
                GT0_TXUSRCLK2_OUT   => TX_User_Clock(quad),
                GT0_RXUSRCLK_OUT    => open,
                GT0_RXUSRCLK2_OUT   => RX_User_Clock(quad),
         
                --_________________________________________________________________________
                --GT0  (X0Y2)
                --____________________________CHANNEL PORTS________________________________
                ---------------------------- Channel - DRP Ports  --------------------------
                gt0_drpaddr_in                  =>      (others => '0'),
                gt0_drpdi_in                    =>      (others => '0'),
                gt0_drpdo_out                   =>      open,
                gt0_drpen_in                    =>      '0',
                gt0_drprdy_out                  =>      open,
                gt0_drpwe_in                    =>      '0',
                --------------------------- Digital Monitor Ports --------------------------
                gt0_dmonitorout_out             =>      open,
         
                --------------------- RX Initialization and Reset Ports --------------------
                gt0_eyescanreset_in             =>      '0',
                gt0_rxuserrdy_in                =>      '1',
                -------------------------- RX Margin Analysis Ports ------------------------
                gt0_eyescandataerror_out        =>      open,
                gt0_eyescantrigger_in           =>      '0',
                ------------------ Receive Ports - FPGA RX interface Ports -----------------
                gt0_rxdata_out                  =>      Data_Transceiver_Out(0),
                --------------------------- Receive Ports - RX AFE -------------------------
                gt0_gtxrxp_in                   =>      RX_In_P(0),
                ------------------------ Receive Ports - RX AFE Ports ----------------------
                gt0_gtxrxn_in                   =>      RX_In_N(0),
                --------------------- Receive Ports - RX Equalizer Ports -------------------
                gt0_rxdfelpmreset_in            =>      '0',
                gt0_rxmonitorout_out            =>      open,
                gt0_rxmonitorsel_in             =>      (others => '0'),
                --------------- Receive Ports - RX Fabric Output Control Ports -------------
                gt0_rxoutclkfabric_out          =>      open,
                ---------------------- Receive Ports - RX Gearbox Ports --------------------
                gt0_rxdatavalid_out             =>      RX_Datavalid_Out(0),
                gt0_rxheader_out                =>      RX_Header_Out(0),
                gt0_rxheadervalid_out           =>      RX_Headervalid_Out(0),
                --------------------- Receive Ports - RX Gearbox Ports  --------------------
                gt0_rxgearboxslip_in            =>      RX_Gearboxslip_In(0),
                ------------- Receive Ports - RX Initialization and Reset Ports ------------
                gt0_gtrxreset_in                =>      reset,
                gt0_rxpmareset_in               =>      '0',
                -------------- Receive Ports -RX Initialization and Reset Ports ------------
                gt0_rxresetdone_out             =>      RX_Resetdone_Out(0),
         
                --------------------- TX Initialization and Reset Ports --------------------
                gt0_gttxreset_in                =>      reset,
                gt0_txuserrdy_in                =>      '1',
                ------------------ Transmit Ports - TX Data Path interface -----------------
                gt0_txdata_in                   =>      Data_Transceiver_In(0),
                ---------------- Transmit Ports - TX Driver and OOB signaling --------------
                gt0_gtxtxn_out                  =>      TX_Out_N(0),
                gt0_gtxtxp_out                  =>      TX_Out_P(0),
                ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
                gt0_txoutclkfabric_out          =>      open,
                gt0_txoutclkpcs_out             =>      open,
                --------------------- Transmit Ports - TX Gearbox Ports --------------------
                gt0_txgearboxready_out          =>      TX_Gearboxready(0),
                gt0_txheader_in                 =>      TX_Header_In(0),
                gt0_txstartseq_in               =>      TX_Startseq_In,
                ------------- Transmit Ports - TX Initialization and Reset Ports -----------
                gt0_txresetdone_out             =>      TX_Resetdone_Out(0),
                --____________________________COMMON PORTS________________________________
                GT0_QPLLLOCK_OUT        => open,
                GT0_QPLLREFCLKLOST_OUT  => open,
                GT0_QPLLOUTCLK_OUT      => open,
                GT0_QPLLOUTREFCLK_OUT   => open,
                sysclk_in               => clk100
            );
            
            startseq : process (TX_User_Clock(0))
            begin
                if rising_edge(TX_User_Clock(0)) then
                    if (reset = '1') then
                        TX_Startseq_In <= '0';
                    elsif (TX_Gearboxready(0) = '1') then
                        TX_Startseq_In <= '1';  
                        
                    end if;
                    TX_Gearboxready_p1 <= TX_Gearboxready;
                end if;
            end process;
            
            -- ug476_7Series_Transceivers - P128 - TXGEARBOXREADY Low. But Data still taken on this cycle.
            TX_Gearboxready_Out <= TX_Gearboxready or TX_Gearboxready_p1;
            
        end generate;
    end generate g_quads;
    
    ------------------------------- Gearbox logic -------------------------------------
    g_gearbox: for i in 0 to Lanes-1 generate
        
        rx_gearbox_reset(i/4) <= rst_rxusr_403M;
        
        ------------------------------- RX Gearbox bitslip -- -------------------------------------
        block_sync_sm_0_i  :  entity work.Transceiver_10g_64b67b_BLOCK_SYNC_SM
            generic map
            (
                SH_CNT_MAX          => 64,
                SH_INVALID_CNT_MAX  => 16
                --Lanes               => Lanes
            )
            port map
            (
                -- User Interface
                BLOCKSYNC_OUT             =>    open,
                RXGEARBOXSLIP_OUT         =>    RX_Gearboxslip_In(i),
                RXHEADER_IN               =>    RX_Header_Out(i),
                RXHEADERVALID_IN          =>    RX_Headervalid_Out(i),
 
                -- System Interface
                USER_CLK                  =>    RX_User_Clock(i/4),
                SYSTEM_RESET              =>    rx_gearbox_reset(i/4)
            );
    end generate;
   

end Behavioral;
