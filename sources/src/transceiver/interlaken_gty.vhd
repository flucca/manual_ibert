library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

library unisim;
use unisim.vcomponents.all;


entity interlaken_gty is
    generic(
        Lanes        : positive := 4;    -- Number of Lanes (Transmission channels)
        BondNumberOfLanes : positive := 1;
        CARD_TYPE    : integer := 128;
        GTREFCLKS    : integer := 1
    );
    Port ( 
        reset : in std_logic;
        rst_txusr_403M_s : out std_logic;
        rst_rxusr_403M_s : out std_logic;
        
        --------- 125 MHz input, to transceiver (QSFP4 clock)------------
        GTREFCLK_IN_P : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_IN_N : in std_logic_vector(GTREFCLKS-1 downto 0);
        
        -------- 100 MHz input, Free Running CLK (QDR4 clock) -----------
        clk100 : in std_logic;

        ------------------- GT data in/out ------------------------------
        TX_Out_P  : out std_logic_vector(Lanes-1 downto 0);
        TX_Out_N  : out std_logic_vector(Lanes-1 downto 0);
        RX_In_P   : in std_logic_vector(Lanes-1 downto 0);
        RX_In_N   : in std_logic_vector(Lanes-1 downto 0);
        
        TX_User_Clock_s : out std_logic_vector(Lanes/4-1 downto 0);
        RX_User_Clock_s : out std_logic_vector(Lanes/4-1 downto 0);
        
        loopback_in       : in std_logic_vector(2 downto 0);
        
        Data_Transceiver_In  : in slv_64_array(0 to Lanes-1);
        Data_Transceiver_Out : out slv_64_array(0 to Lanes-1);
        RX_Datavalid_Out : out std_logic_vector(Lanes-1 downto 0);
        RX_Header_Out_s : out slv_3_array(0 to Lanes-1);
        RX_Headervalid_Out_s : out std_logic_vector(Lanes-1 downto 0);
        TX_Gearboxready_Out : out std_logic_vector(Lanes-1 downto 0);
        
        TX_Header_In : in slv_3_array(0 to Lanes-1)
    );
end interlaken_gty;

architecture Behavioral of interlaken_gty is

    -------------------------- Include Transceiver -----------------------------
--    COMPONENT gtwizard_ultrascale_0
--    PORT (
--        gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
--        gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
--        gtrefclk00_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        qpll0outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        qpll0outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
--        rxgearboxslip_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        txheader_in : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
--        txsequence_in : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
--        txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxdatavalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--        rxheader_out : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
--        rxheadervalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--        rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        rxstartofseq_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--        txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--        txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
--    );
--    END COMPONENT;

    --signal TX_User_Clock, RX_User_Clock : std_logic;
    signal TX_User_Clock, RX_User_Clock : std_logic_vector(Lanes/4-1 downto 0);
    
    signal RX_Header_Out : slv_3_array(0 to Lanes-1);
    signal RX_Headervalid_Out : std_logic_vector(Lanes-1 downto 0);
    signal RX_Gearboxslip_In : std_logic_vector(Lanes-1 downto 0);
    signal not_RX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal RX_Resetdone_Out is never read"
    
    signal not_TX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal TX_Resetdone_Out is never read"
    
    signal  gt_txsequence_i         : slv_7_array(0 to Lanes-1);
    signal  gt_txseq_counter_r      : uns_9_array(0 to Lanes-1);

    signal gt_pause_data_valid_r : std_logic_vector(Lanes-1 downto 0);
    signal gt_data_valid_out_i   : std_logic_vector(Lanes-1 downto 0);

    signal GTREFCLK : std_logic_vector((Lanes/4)-1 downto 0);

     ---- GTY added signals -------
     signal txoutclk_out, rxoutclk_out: std_logic_vector(Lanes-1 downto 0);
--     signal tx_active_sync, tx_active_meta : std_logic;
--     signal rx_active_sync, rx_active_meta : std_logic;
     signal gtwiz_userclk_rx_active_in ,gtwiz_userclk_tx_active_in : std_logic_vector((Lanes/4)-1 downto 0);
     signal tx_gearbox_reset, rx_gearbox_reset : std_logic_vector((Lanes/4)-1 downto 0);
     signal rst_txusr_403M, rst_rxusr_403M : std_logic;
     signal gtwiz_reset_rx_done_out : std_logic_vector((Lanes/4)-1 downto 0);
     
     signal GTREFCLK_VERSAL_BUF, GTREFCLK_VERSAL_SEL: std_logic_vector(Lanes/4-1 downto 0);
     
begin
    
--    gtwiz_userclk_tx_active_in <= (others => tx_active_sync);
--    gtwiz_userclk_rx_active_in <= (others => rx_active_sync);
    
    RX_Header_Out_s <= RX_Header_Out;
    RX_Headervalid_Out_s <= RX_Headervalid_Out;
    TX_User_Clock_s <= TX_User_Clock;
    RX_User_Clock_s <= RX_User_Clock;
    rst_txusr_403M_s <= rst_txusr_403M;
    rst_rxusr_403M_s <= rst_rxusr_403M;
    
    INST_RST_TXUSR : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => rst_txusr_403M, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        dest_clk => TX_User_Clock(0),   -- 1-bit input: Destination clock.
        src_arst => reset    -- 1-bit input: Source asynchronous reset signal.
    );
    INST_RST_RXUSR : xpm_cdc_async_rst
    generic map (
        DEST_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        RST_ACTIVE_HIGH => 1  -- DECIMAL; 0=active low reset, 1=active high reset
    )
    port map (
        dest_arst => rst_rxusr_403M, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain. 
        dest_clk => RX_User_Clock(0),   -- 1-bit input: Destination clock.
        src_arst => reset    -- 1-bit input: Source asynchronous reset signal.
    );
    
    GTREFCLK_VERSAL_SEL(0) <= GTREFCLK_VERSAL_BUF(0);
    g_2quads: if Lanes/4 > 1 generate
        GTREFCLK_VERSAL_SEL(1) <= GTREFCLK_VERSAL_BUF(1);
    end generate;
    g_3quads: if Lanes/4 > 2 generate
        GTREFCLK_VERSAL_SEL(2) <= GTREFCLK_VERSAL_BUF(2);
    end generate;
    g_4quads: if Lanes/4 > 3 generate
        GTREFCLK_VERSAL_SEL(3) <= GTREFCLK_VERSAL_BUF(3);
    end generate;
    
    g_quads: for quad in 0 to Lanes/4 -1 generate
        signal gtwiz_userdata_tx_in, gtwiz_userdata_rx_out: std_logic_vector(255 downto 0);
        signal gtyrxn_in, gtyrxp_in : std_logic_vector(3 downto 0);
        signal gtytxn_out, gtytxp_out : std_logic_vector(3 downto 0);
        signal rxgearboxslip_in : std_logic_vector(3 downto 0);
        
        signal ch0_txdata_ext_0, ch1_txdata_ext_0, ch2_txdata_ext_0, ch3_txdata_ext_0: std_logic_vector ( 127 downto 0 );
        
        signal txpmaresetdone_out, rxpmaresetdone_out: std_logic_vector(3 downto 0);
        signal gtwiz_reset_all_in : std_logic_vector(0 downto 0);
        signal rxdatavalid_out, rxheadervalid_out : std_logic_vector(7 downto 0);
        signal rxusrclk_in, rxusrclk2_in : std_logic_vector(3 downto 0);
        signal txusrclk_in, txusrclk2_in : std_logic_vector(3 downto 0);
        signal gtwiz_reset_clk_freerun_in : std_logic_vector(0 downto 0);
        --signal gt_rxsequence_o : std_logic_vector(7 downto 0);
        signal txheader_in, rxheader_out : std_logic_vector(23 downto 0);
        signal txsequence_in: std_logic_vector(27 downto 0);
        signal loopback: std_logic_vector(11 downto 0);
        
        signal tx_active_sync, tx_active_meta : std_logic;
        signal rx_active_sync, rx_active_meta : std_logic;
        signal tx_gearbox_reset_i : std_logic;
        signal rx_gearbox_reset_i : std_logic;
        signal not_TX_Resetdone_Out_tx_User_clock, not_RX_Resetdone_Out_rx_User_clock: std_logic;
    begin
        gtwiz_userclk_tx_active_in(quad downto quad) <= (others => tx_active_sync);
        gtwiz_userclk_rx_active_in(quad downto quad) <= (others => rx_active_sync);
        
        gtwiz_userdata_tx_in(63 downto 0) <= Data_Transceiver_In(quad*4+0);    
        gtwiz_userdata_tx_in(127 downto 64) <= Data_Transceiver_In(quad*4+1);    
        gtwiz_userdata_tx_in(191 downto 128) <= Data_Transceiver_In(quad*4+2);    
        gtwiz_userdata_tx_in(255 downto 192) <= Data_Transceiver_In(quad*4+3);    
        
        Data_Transceiver_Out(quad*4 + 0) <= gtwiz_userdata_rx_out(63 downto 0);
        Data_Transceiver_Out(quad*4 + 1) <= gtwiz_userdata_rx_out(127 downto 64);
        Data_Transceiver_Out(quad*4 + 2) <= gtwiz_userdata_rx_out(191 downto 128);
        Data_Transceiver_Out(quad*4 + 3) <= gtwiz_userdata_rx_out(255 downto 192);
        
        gtyrxn_in <= RX_In_N(quad*4+3 downto quad*4);
        gtyrxp_in <= RX_In_P(quad*4+3 downto quad*4);
        TX_Out_N(quad*4+3 downto quad*4) <= gtytxn_out;
        TX_Out_P(quad*4+3 downto quad*4) <= gtytxp_out;
        
        rxgearboxslip_in <= RX_Gearboxslip_In(quad*4+3 downto quad*4);
        txusrclk_in <= (others => TX_User_Clock(quad));
        txusrclk2_in <= txusrclk_in; --Datapath 64b and intw 2; ug578 p105
        rxusrclk_in <= (others => RX_User_Clock(quad));
        rxusrclk2_in <= rxusrclk_in; --Datapath 64b and intw 2; ug578 p105
        
        --TEMPREMOVE
        -- Doesn't work since clk100 is inactive when txpmareset deasserts. So output always stays high
--        TX_Resetdone_Out <= not NOT_TX_Resetdone_Out;
--        RX_Resetdone_Out <= not NOT_RX_Resetdone_Out;
--        sync_RX_Resetdone : xpm_cdc_array_single
--           generic map (
--              DEST_SYNC_FF => 2,
--              INIT_SYNC_FF => 0,
--              SIM_ASSERT_CHK => 0,
--              SRC_INPUT_REG => 0,
--              WIDTH => 4
--           )
--           port map (
--              dest_out => NOT_RX_Resetdone_Out(quad*4+3 downto quad*4),
--              dest_clk => RX_User_Clock(quad),
--              src_clk => clk100,
--              src_in => rxpmaresetdone_out
--           );
        
--        sync_TX_Resetdone : xpm_cdc_array_single
--           generic map (
--              DEST_SYNC_FF => 2,
--              INIT_SYNC_FF => 0,
--              SIM_ASSERT_CHK => 0,
--              SRC_INPUT_REG => 0,
--              WIDTH => 4
--           )
--           port map (
--              dest_out => NOT_TX_Resetdone_Out(quad*4+3 downto quad*4),
--              dest_clk => TX_User_Clock(quad),
--              src_clk => clk100,
--              src_in => txpmaresetdone_out
--           );
           
--        NOT_TX_Resetdone_Out(quad*4+3 downto quad*4) <= not TX_Resetdone_Out(quad*4+3 downto quad*4);
--        NOT_RX_Resetdone_Out(quad*4+3 downto quad*4) <= not RX_Resetdone_Out(quad*4+3 downto quad*4);
        
        not_TX_Resetdone_Out(quad*4+3 downto quad*4) <= not txpmaresetdone_out;
        not_RX_Resetdone_Out(quad*4+3 downto quad*4) <= not rxpmaresetdone_out;
        
        gtwiz_reset_all_in <= (others => reset);
        RX_Datavalid_Out(quad*4+0 ) <= rxdatavalid_out(0);
        RX_Datavalid_Out(quad*4+1 ) <= rxdatavalid_out(2);
        RX_Datavalid_Out(quad*4+2 ) <= rxdatavalid_out(4);
        RX_Datavalid_Out(quad*4+3 ) <= rxdatavalid_out(6);
        
        RX_Headervalid_Out(quad*4+0) <= rxheadervalid_out(0);
        RX_Headervalid_Out(quad*4+1) <= rxheadervalid_out(2);
        RX_Headervalid_Out(quad*4+2) <= rxheadervalid_out(4);
        RX_Headervalid_Out(quad*4+3) <= rxheadervalid_out(6);
        
        gtwiz_reset_clk_freerun_in(0) <= clk100;
    
        txheader_in(5 downto 0)   <= "000" & TX_Header_In(quad*4+0);
        txheader_in(11 downto 6)  <= "000" & TX_Header_In(quad*4+1);
        txheader_in(17 downto 12) <= "000" & TX_Header_In(quad*4+2);
        txheader_in(23 downto 18) <= "000" & TX_Header_In(quad*4+3);
        RX_Header_Out(quad*4+0)   <= rxheader_out(2 downto 0);
        RX_Header_Out(quad*4+1)   <= rxheader_out(8 downto 6);
        RX_Header_Out(quad*4+2)   <= rxheader_out(14 downto 12);
        RX_Header_Out(quad*4+3)   <= rxheader_out(20 downto 18);
        txsequence_in <= gt_txsequence_i(quad*4+3)&
                         gt_txsequence_i(quad*4+2)&
                         gt_txsequence_i(quad*4+1)&
                         gt_txsequence_i(quad*4+0);
                         
        
        loopback <= loopback_in&loopback_in&loopback_in&loopback_in;
        
        g_ultrascale: if CARD_TYPE = 128 generate
            -------------------------- Include Transceiver -----------------------------
            COMPONENT gtwizard_ultrascale_0
              PORT (
                gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
                gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
                gtrefclk00_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                qpll0outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                qpll0outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
                rxgearboxslip_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                txheader_in : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
                txsequence_in : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
                txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxdatavalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                rxheader_out : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
                rxheadervalid_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                rxstartofseq_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
              );
            END COMPONENT;
        begin
        gtwizard_ultrascale_0_i : gtwizard_ultrascale_0
        PORT MAP (
            loopback_in => loopback,
            gtyrxn_in => gtyrxn_in,
            gtyrxp_in => gtyrxp_in,
            gtytxn_out => gtytxn_out,
            gtytxp_out => gtytxp_out,
      
            gtwiz_userclk_tx_active_in => gtwiz_userclk_tx_active_in(quad downto quad),
            gtwiz_userclk_rx_active_in => gtwiz_userclk_rx_active_in(quad downto quad),
            
            gtwiz_reset_clk_freerun_in => gtwiz_reset_clk_freerun_in,
            gtwiz_reset_all_in => gtwiz_reset_all_in,
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_tx_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_datapath_in => "0",
            gtwiz_reset_rx_cdr_stable_out => open,
            gtwiz_reset_tx_done_out => open,
            gtwiz_reset_rx_done_out => gtwiz_reset_rx_done_out(quad downto quad),
            
            
            gtrefclk00_in => GTREFCLK(quad downto quad),
            qpll0outclk_out => open,
            qpll0outrefclk_out => open,
            txusrclk_in => txusrclk_in,
            txusrclk2_in => txusrclk2_in,
            rxusrclk_in => rxusrclk_in,
            rxusrclk2_in => rxusrclk2_in,
            
            txoutclk_out => txoutclk_out(quad*4+3 downto quad*4),
            rxoutclk_out => rxoutclk_out(quad*4+3 downto quad*4),
            
            gtwiz_userdata_tx_in => gtwiz_userdata_tx_in,--Data_Transceiver_In(0),
            gtwiz_userdata_rx_out => gtwiz_userdata_rx_out, --Data_Transceiver_Out(0),
            txheader_in => txheader_in,
            txsequence_in => txsequence_in,
            
            
            
            gtpowergood_out => open,
            
            rxdatavalid_out   => rxdatavalid_out,
            rxheader_out      => rxheader_out,
            rxheadervalid_out => rxheadervalid_out,
            rxgearboxslip_in  => rxgearboxslip_in,
            
            rxpmaresetdone_out => rxpmaresetdone_out,
            rxstartofseq_out   => open, --gt_rxsequence_o,
            
            txpmaresetdone_out => txpmaresetdone_out
        );
     --end generate;   
        ------------------------------- Buffering tx/rx out clock signals --------------------------------
    --g_clockbuffers: for i in 0 to Lanes-1 generate
        BUFG_GT_TXclk : BUFG_GT
        port map (
          O => TX_User_Clock(quad),
          CE => '1',
          CEMASK => '0',
          CLR => not_TX_Resetdone_Out(quad*4), --NOT_TX_Resetdone_Out
          CLRMASK => '0',
          DIV => "000",
          I => txoutclk_out(quad*4)
        );
        
      
        BUFG_GT_RXclk : BUFG_GT
        port map (
          O => RX_User_Clock(quad), 
          CE => '1',  
          CEMASK => '0',
          CLR => not_RX_Resetdone_Out(quad*4),
          CLRMASK => '0', 
          DIV => "000",
          I => rxoutclk_out(quad*4) 
        );
    --end generate;
    
    -------------------------------- Buffering QSFP GT clock -------------------------------------
    IBUFDS_GTE4_inst : IBUFDS_GTE4
    generic map (
       REFCLK_EN_TX_PATH => '0',  
       REFCLK_HROW_CK_SEL => "00",
       REFCLK_ICNTL_RX => "00"    
    )
    port map (
       O => GTREFCLK(quad),       
       ODIV2 => open, 
       CEB => '0',    
       I => GTREFCLK_IN_P(quad),    
       IB => GTREFCLK_IN_N(quad)    
    );
    end generate g_ultrascale;
    
        g_versalprime: if CARD_TYPE = 180 or CARD_TYPE = 181 generate
            component transceiver_versal_interlaken_wrapper is
              port (
                GT_REFCLK0 : in STD_LOGIC;
                GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
                apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
                apb3clk_quad : in STD_LOGIC;
                ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch0_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch0_rxcdrlock_ext_0 : out STD_LOGIC;
                ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch0_rxpolarity_ext_0 : in STD_LOGIC;
                ch0_rxresetdone_ext_0 : out STD_LOGIC;
                ch0_rxslide_ext_0 : in STD_LOGIC;
                ch0_rxvalid_ext_0 : out STD_LOGIC;
                ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_txpolarity_ext_0 : in STD_LOGIC;
                ch0_txresetdone_ext_0 : out STD_LOGIC;
                ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch1_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch1_rxcdrlock_ext_0 : out STD_LOGIC;
                ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch1_rxpolarity_ext_0 : in STD_LOGIC;
                ch1_rxresetdone_ext_0 : out STD_LOGIC;
                ch1_rxslide_ext_0 : in STD_LOGIC;
                ch1_rxvalid_ext_0 : out STD_LOGIC;
                ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_txpolarity_ext_0 : in STD_LOGIC;
                ch1_txresetdone_ext_0 : out STD_LOGIC;
                ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch2_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch2_rxcdrlock_ext_0 : out STD_LOGIC;
                ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch2_rxpolarity_ext_0 : in STD_LOGIC;
                ch2_rxresetdone_ext_0 : out STD_LOGIC;
                ch2_rxslide_ext_0 : in STD_LOGIC;
                ch2_rxvalid_ext_0 : out STD_LOGIC;
                ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_txpolarity_ext_0 : in STD_LOGIC;
                ch2_txresetdone_ext_0 : out STD_LOGIC;
                ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch3_rxbyteisaligned_ext_0 : out STD_LOGIC;
                ch3_rxcdrlock_ext_0 : out STD_LOGIC;
                ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
                ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxpmaresetdone_ext_0 : out STD_LOGIC;
                ch3_rxpolarity_ext_0 : in STD_LOGIC;
                ch3_rxresetdone_ext_0 : out STD_LOGIC;
                ch3_rxslide_ext_0 : in STD_LOGIC;
                ch3_rxvalid_ext_0 : out STD_LOGIC;
                ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_txpolarity_ext_0 : in STD_LOGIC;
                ch3_txresetdone_ext_0 : out STD_LOGIC;
                ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
                lcpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
                link_status_gt_bridge_ip_0 : out STD_LOGIC;
                rate_sel_gt_bridge_ip_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
                reset_rx_datapath_in_0 : in STD_LOGIC;
                reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
                reset_tx_datapath_in_0 : in STD_LOGIC;
                reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
                rpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
                rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                rxusrclk_gt_bridge_ip_0 : out STD_LOGIC;
                tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                txusrclk_gt_bridge_ip_0 : out STD_LOGIC
              );
            end component transceiver_versal_interlaken_wrapper;
            signal ch0_rxdata_ext_0: std_logic_vector(127 downto 0);
            signal ch1_rxdata_ext_0: std_logic_vector(127 downto 0);
            signal ch2_rxdata_ext_0: std_logic_vector(127 downto 0);
            signal ch3_rxdata_ext_0: std_logic_vector(127 downto 0);
        begin
        IBUFDS_GTE5_inst : IBUFDS_GTE5
            generic map (
               REFCLK_EN_TX_PATH => '0',  
               REFCLK_HROW_CK_SEL => 0,
               REFCLK_ICNTL_RX => 0    
            )
            port map (
               O => GTREFCLK_VERSAL_BUF(quad),       
               ODIV2 => open, 
               CEB => '0',    
               I => GTREFCLK_IN_P(quad),    
               IB => GTREFCLK_IN_N(quad)    
            );
            
        
        gtwiz_userdata_rx_out(63 downto 0)    <= ch0_rxdata_ext_0(63 downto 0);
        gtwiz_userdata_rx_out(127 downto 64)  <= ch1_rxdata_ext_0(63 downto 0);
        gtwiz_userdata_rx_out(191 downto 128) <= ch2_rxdata_ext_0(63 downto 0);
        gtwiz_userdata_rx_out(255 downto 192) <= ch3_rxdata_ext_0(63 downto 0);
        
        ch0_txdata_ext_0 <= x"0000_0000_0000_0000" & gtwiz_userdata_tx_in(63 downto 0);
        ch1_txdata_ext_0 <= x"0000_0000_0000_0000"&gtwiz_userdata_tx_in(127 downto 64);
        ch2_txdata_ext_0 <= x"0000_0000_0000_0000"&gtwiz_userdata_tx_in(191 downto 128);
        ch3_txdata_ext_0 <= x"0000_0000_0000_0000"&gtwiz_userdata_tx_in(255 downto 192);
        
        quad0: transceiver_versal_interlaken_wrapper 
              port map(
                GT_REFCLK0                       => GTREFCLK_VERSAL_SEL(quad),
                GT_Serial_grx_n                  => gtyrxn_in,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_grx_p                  => gtyrxp_in,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_n                  => gtytxn_out,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                GT_Serial_gtx_p                  => gtytxp_out,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                apb3clk_gt_bridge_ip_0           => clk100,--: in STD_LOGIC;
                apb3clk_quad                     => clk100,--: in STD_LOGIC;
                ch0_loopback_0                   => loopback(2 downto 0),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch0_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch0_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch0_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_rxdata_ext_0                 => ch0_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_rxdatavalid_ext_0            => rxdatavalid_out(1 downto 0),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxgearboxslip_ext_0          => rxgearboxslip_in(0),--: in STD_LOGIC;
                ch0_rxheader_ext_0               => rxheader_out(5 downto 0),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_rxheadervalid_ext_0          => rxheadervalid_out(1 downto 0),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch0_rxpmaresetdone_ext_0         => rxpmaresetdone_out(0),--: out STD_LOGIC;
                ch0_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch0_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch0_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch0_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch0_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch0_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_txdata_ext_0                 => ch0_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch0_txheader_ext_0               => txheader_in(5 downto 0),--: in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch0_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch0_txresetdone_ext_0            => txpmaresetdone_out(0),--: out STD_LOGIC;
                ch0_txsequence_ext_0             => txsequence_in(6 downto 0),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch1_loopback_0                   => loopback(5 downto 3),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch1_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch1_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch1_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_rxdata_ext_0                 => ch1_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_rxdatavalid_ext_0            => rxdatavalid_out(3 downto 2),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxgearboxslip_ext_0          => rxgearboxslip_in(1),--: in STD_LOGIC;
                ch1_rxheader_ext_0               => rxheader_out(11 downto 6),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_rxheadervalid_ext_0          => rxheadervalid_out(3 downto 2),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch1_rxpmaresetdone_ext_0         => rxpmaresetdone_out(1),--: out STD_LOGIC;
                ch1_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch1_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch1_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch1_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch1_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch1_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch1_txdata_ext_0                 => ch1_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch1_txheader_ext_0               => txheader_in(11 downto 6), -- in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch1_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch1_txresetdone_ext_0            => txpmaresetdone_out(1),--: out STD_LOGIC;
                ch1_txsequence_ext_0             => txsequence_in(13 downto 7),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch2_loopback_0                   => loopback(8 downto 6),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch2_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch2_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch2_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_rxdata_ext_0                 => ch2_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_rxdatavalid_ext_0            => rxdatavalid_out(5 downto 4),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxgearboxslip_ext_0          => rxgearboxslip_in(2),--: in STD_LOGIC;
                ch2_rxheader_ext_0               => rxheader_out(17 downto 12),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_rxheadervalid_ext_0          => rxheadervalid_out(5 downto 4),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch2_rxpmaresetdone_ext_0         => rxpmaresetdone_out(2),--: out STD_LOGIC;
                ch2_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch2_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch2_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch2_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch2_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch2_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch2_txdata_ext_0                 => ch2_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch2_txheader_ext_0               => txheader_in(17 downto 12), -- in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch2_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch2_txresetdone_ext_0            => txpmaresetdone_out(2),--: out STD_LOGIC;
                ch2_txsequence_ext_0             => txsequence_in(20 downto 14),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                ch3_loopback_0                   => loopback(11 downto 9),--: in STD_LOGIC_VECTOR ( 2 downto 0 );
                ch3_rxbyteisaligned_ext_0        => open,--: out STD_LOGIC;
                ch3_rxcdrlock_ext_0              => open,--: out STD_LOGIC;
                ch3_rxctrl0_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl1_ext_0                => open,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_rxctrl2_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxctrl3_ext_0                => open,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_rxdata_ext_0                 => ch3_rxdata_ext_0,--: out STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_rxdatavalid_ext_0            => rxdatavalid_out(7 downto 6),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxgearboxslip_ext_0          => rxgearboxslip_in(3),--: in STD_LOGIC;
                ch3_rxheader_ext_0               => rxheader_out(23 downto 18),--: out STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_rxheadervalid_ext_0          => rxheadervalid_out(7 downto 6),--: out STD_LOGIC_VECTOR ( 1 downto 0 );
                ch3_rxpmaresetdone_ext_0         => rxpmaresetdone_out(3),--: out STD_LOGIC;
                ch3_rxpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch3_rxresetdone_ext_0            => open,--: out STD_LOGIC;
                ch3_rxslide_ext_0                => '0',--: in STD_LOGIC;
                ch3_rxvalid_ext_0                => open,--: out STD_LOGIC;
                ch3_txctrl0_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl1_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                ch3_txctrl2_ext_0                => (others => '0'),--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                ch3_txdata_ext_0                 => ch3_txdata_ext_0,--: in STD_LOGIC_VECTOR ( 127 downto 0 );
                ch3_txheader_ext_0               => txheader_in(23 downto 18), -- in STD_LOGIC_VECTOR ( 5 downto 0 );
                ch3_txpolarity_ext_0             => '0',--: in STD_LOGIC;
                ch3_txresetdone_ext_0            => txpmaresetdone_out(3),--: out STD_LOGIC;
                ch3_txsequence_ext_0             => txsequence_in(27 downto 21),--: in STD_LOGIC_VECTOR ( 6 downto 0 );
                gt_reset_gt_bridge_ip_0          => gtwiz_reset_all_in(0), --: in STD_LOGIC;
                lcpll_lock_gt_bridge_ip_0        => open,--: out STD_LOGIC;
                link_status_gt_bridge_ip_0       => open,--: out STD_LOGIC;
                rate_sel_gt_bridge_ip_0          => "0000",--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                reset_rx_datapath_in_0           => '0',--: in STD_LOGIC;
                reset_rx_pll_and_datapath_in_0   => '0',--: in STD_LOGIC;
                reset_tx_datapath_in_0           => '0',--: in STD_LOGIC;
                reset_tx_pll_and_datapath_in_0   => '0',--: in STD_LOGIC;
                rpll_lock_gt_bridge_ip_0         => open,--: out STD_LOGIC;
                rx_resetdone_out_gt_bridge_ip_0  => gtwiz_reset_rx_done_out(quad),--: out STD_LOGIC;
                rxusrclk_gt_bridge_ip_0          => rx_user_clock_s(quad),--: out STD_LOGIC;
                tx_resetdone_out_gt_bridge_ip_0  => open,--: out STD_LOGIC;
                txusrclk_gt_bridge_ip_0          => tx_user_clock_s(quad) --: out STD_LOGIC
              );
    end generate g_versalprime;
    
    xpm_cdc_sync_rst_inst_not_tx_resetdone_out : xpm_cdc_sync_rst
    generic map (
       DEST_SYNC_FF => 2,
       INIT => 1,
       INIT_SYNC_FF => 0,
       SIM_ASSERT_CHK => 0
    )
    port map (
       dest_rst => not_TX_Resetdone_Out_tx_User_clock,
       dest_clk => TX_User_Clock(quad),
       src_rst => not_TX_Resetdone_Out(quad)
    );
    xpm_cdc_sync_rst_inst_not_rx_resetdone_out : xpm_cdc_sync_rst
    generic map (
       DEST_SYNC_FF => 2,
       INIT => 1,
       INIT_SYNC_FF => 0,
       SIM_ASSERT_CHK => 0
    )
    port map (
       dest_rst => not_RX_Resetdone_Out_rx_User_clock,
       dest_clk => RX_User_Clock(quad),
       src_rst => not_RX_Resetdone_Out(quad)
    );
    ------------------------------ Set GTY active signals ---------------------------------------
    --! FS: TX_Resetdone_Out and RX_Resetdone_out prepended with not_ because they are inverted
    --! FS: Syncronized the signals with xpm_cdc_sync_rst to RX/TX user_clock to avoid timing violations 
    tx_active : process (TX_User_Clock, not_TX_Resetdone_Out_tx_User_clock) 
    begin
        if not_TX_Resetdone_Out_tx_User_clock = '1' then
                tx_active_meta <= '0';
                tx_active_sync <= '0';
        elsif rising_edge(TX_User_Clock(quad)) then
                tx_active_meta <= '1';
                tx_active_sync <= tx_active_meta;
        end if;
    end process;
    
    rx_active : process (RX_User_Clock, not_RX_Resetdone_Out_rx_User_clock) 
    begin
        if not_RX_Resetdone_Out_rx_User_clock = '1' then
                rx_active_meta <= '0';
                rx_active_sync <= '0';
        elsif rising_edge(RX_User_Clock(quad)) then
            
            
                rx_active_meta <= '1';
                rx_active_sync <= rx_active_meta;
            
        end if;
    end process;
    
    ------------------------------- Gearbox reset -------------------------------------
--    tx_gearbox_reset <= rst_txusr_403M or not gtwiz_userclk_tx_active_in(0); -- tx_gearbox_reset <= reset or not gtwiz_userclk_tx_active_in(0);
--    rx_gearbox_reset <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(0);
--    tx_gearbox_reset_i <= reset or not gtwiz_userclk_tx_active_in(quad);
--    rx_gearbox_reset_i <= reset or not gtwiz_reset_rx_done_out(quad);

--    tx_gearbox_reset(quad) <= rst_txusr_403M or not gtwiz_userclk_tx_active_in(quad);
--    rx_gearbox_reset(quad) <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(quad);
    tx_gearbox_reset_i <= rst_txusr_403M or not gtwiz_userclk_tx_active_in(quad);
    rx_gearbox_reset_i <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(quad);
    
    tx_gearbox_reset_sync_inst : xpm_cdc_single
           generic map (
              DEST_SYNC_FF => 2,   
              INIT_SYNC_FF => 0,   
              SIM_ASSERT_CHK => 0, 
              SRC_INPUT_REG => 0 
           )                       
           port map (              
              dest_out => tx_gearbox_reset(quad),
              dest_clk => TX_User_Clock(quad),
              src_clk => clk100,  
              src_in => tx_gearbox_reset_i     
           );
        
        rx_gearbox_reset_sync_inst : xpm_cdc_single
           generic map (
              DEST_SYNC_FF => 2,   
              INIT_SYNC_FF => 0,   
              SIM_ASSERT_CHK => 0, 
              SRC_INPUT_REG => 0 
           )                       
           port map (              
              dest_out => rx_gearbox_reset(quad),
              dest_clk => RX_User_Clock(quad),
              src_clk => clk100,  
              src_in => rx_gearbox_reset_i     
           );
        
    end generate g_quads;
    
    ------------------------------- Gearbox logic -------------------------------------
    g_gearbox: for i in 0 to Lanes-1 generate
        gt_data_valid_out_i(i) <=  '1' when ((gt_txsequence_i(i) /= "0010101") and (gt_txsequence_i(i) /= "0101011") and (gt_txsequence_i(i) /= "1000001")) else
                                        '0';
                                        
        process(TX_User_Clock)
        begin
            if rising_edge (TX_User_Clock(i/4)) then
                gt_pause_data_valid_r(i) <=  gt_data_valid_out_i(i) ;
            end if;
        end process;

        TX_Gearboxready_Out(i)  <= '1' when (gt_pause_data_valid_r(i)='1') else '0';

        ------------------------------- TX Gearbox sequencer -------------------------------------  
        process(TX_User_Clock)
        begin
            if rising_edge (TX_User_Clock(i/4)) then
                if((tx_gearbox_reset(i/4)='1') or (gt_txseq_counter_r(i) = 133)) then--66)) then
                    gt_txseq_counter_r(i) <=  (others => '0') ;
                else
                    gt_txseq_counter_r(i) <=  gt_txseq_counter_r(i) + 1 ;
                end if;
            end if;
        end process;
        gt_txsequence_i(i)         <= std_logic_vector(gt_txseq_counter_r(i)(7 downto 1)); --(6 downto 0));
        
        --rx_gearbox_reset <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(0); --rx_gearbox_reset <= reset or not gtwiz_reset_rx_done_out(0);
        
        ------------------------------- RX Gearbox bitslip -- -------------------------------------
        block_sync_sm_0_i  :  entity work.Transceiver_10g_64b67b_BLOCK_SYNC_SM
            generic map
            (
                SH_CNT_MAX          => 64,
                SH_INVALID_CNT_MAX  => 16
                --Lanes               => Lanes
            )
            port map
            (
                -- User Interface
                BLOCKSYNC_OUT             =>    open,
                RXGEARBOXSLIP_OUT         =>    RX_Gearboxslip_In(i),
                RXHEADER_IN               =>    RX_Header_Out(i),
                RXHEADERVALID_IN          =>    RX_Headervalid_Out(i),
 
                -- System Interface
                USER_CLK                  =>    RX_User_Clock(i/4),
                SYSTEM_RESET              =>    rx_gearbox_reset(i/4)
            );
    end generate;

end Behavioral;
