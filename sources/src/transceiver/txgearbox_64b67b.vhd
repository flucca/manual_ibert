----------------------------------------------------------------------------------
-- Company: Argonne National Laboratory
-- Engineer: Tong Xu and Alexander Paramonov
-- 
-- Create Date: 01/31/2022 04:10:02 PM
-- Design Name: 
-- Module Name: txgearbox_64b67b - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity txgearbox_64b67b is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;           
           data_in : in STD_LOGIC_VECTOR (66 downto 0);
           data_valid_in : STD_LOGIC;
           txsequence_in : in STD_LOGIC_VECTOR (6 downto 0);
           data_out : out STD_LOGIC_VECTOR (63 downto 0)
           );
end txgearbox_64b67b;

architecture Behavioral of txgearbox_64b67b is

constant MAX_INPUT : integer := 67;
constant MAX_OUTPUT : integer := 64;
signal txsequence_in_valid : std_logic := '0';
signal data_in_pl : std_logic_vector(MAX_INPUT-1 downto 0);
signal buf : std_logic_vector((MAX_OUTPUT + 3*28)-1 downto 0);
signal BitsInBuf : integer range 0 to 67 := 0;

begin

    data_assign: for i in MAX_INPUT-1 downto 0 generate
        data_in_pl(i) <= data_in(MAX_INPUT-i-1);
    end generate;

process(clk,reset)
variable data_out_v : std_logic_vector(MAX_OUTPUT-1 downto 0);
begin
        if rising_edge(clk) then
            if((txsequence_in /= "0010101") and (txsequence_in /= "0101011") and (txsequence_in /= "1000001")) then
                txsequence_in_valid <= '1';
            else
                txsequence_in_valid <= '0';
            end if;
            
            if(reset = '1') then
               data_out <= (others => '0');
               BitsInBuf <= 0;
            else
              if(txsequence_in_valid = '1') then
                    BitsInBuf <= BitsInBuf + MAX_INPUT - MAX_OUTPUT;
                    for i in buf'high downto 0 loop
                        if(i >= MAX_INPUT) then
                            buf(i) <= buf(i-MAX_INPUT);
                        else
                            buf(i) <= data_in_pl(MAX_INPUT-i-1); --LSB comes in first and it comes out first
                        end if;
                    end loop;
               else
                    BitsInBuf <= BitsInBuf - MAX_OUTPUT;
               end if;
               for i in MAX_OUTPUT-1 downto 0 loop
                    data_out_v(i) := buf(BitsInBuf + (MAX_OUTPUT -1 -i));  --LSB comes in first and it comes out first
               end loop;
               data_out <= data_out_v;
            end if;
    end if;
end process;

end Behavioral;
