----------------------------------------------------------------------------------
-- Company: 
library ieee;
use ieee.std_logic_1164.all;
library unisim;
use unisim.vcomponents.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

entity interlaken_top is
    generic (
        lanes            : positive := 4 
    );
    port(

        -- system clock 300MHz
        SYS_CLK_300_P : in std_logic;
        SYS_CLK_300_N : in std_logic;

        ---- Freerunning 100 MHz clock
        --qdr4_clk_p : in std_logic;
        --qdr4_clk_n : in std_logic;
        
        -- GTY 156,25 MHz clock 
        qsfp4_clock_p : in std_logic;
        qsfp4_clock_n : in std_logic;
    
        -- QSFP4 data signals
        gt_rx_n : in std_logic_vector(lanes-1 downto 0);
        gt_rx_p : in std_logic_vector(lanes-1 downto 0);
        gt_tx_n : out std_logic_vector(lanes-1 downto 0);
        gt_tx_p : out std_logic_vector(lanes-1 downto 0)
    
--		Lock_Out  : out std_logic;
--		Valid_out : out std_logic
    );
end entity interlaken_top;

architecture Test of interlaken_top is
    
    --constant    Lanes                   : integer              :=  4;
    constant    BurstMax                : positive             := 256;                -- Configurable value of BurstMax
    constant    BurstShort              : positive             := 64;                 -- Configurable value of BurstShort
    constant    PacketLength            : positive             := 256;                -- Configurable value of PacketLength of META frame-- 24 packets * 8  = 192 B
    constant    CLOCKING_MODE           : string               := "independent_clock";
    constant    RELATED_CLOCKS          : integer range 0 to 1 := 0;
    constant    FIFO_MEMORY_TYPE        : string               := "auto";
    constant    PACKET_FIFO             : string               := "false";
    constant    BondNumberOfLanes       : positive := 1;
    
    signal TX_Out_P_s        : std_logic_vector(lanes-1  downto 0);
    signal TX_Out_N_s        : std_logic_vector(lanes-1  downto 0);
    signal RX_In_P_s         : std_logic_vector(lanes-1  downto 0);
    signal RX_In_N_s         : std_logic_vector(lanes-1  downto 0);
    signal clk100            : std_logic;
    signal clk150            : std_logic;
    signal clk300            : std_logic;
    signal m_axis_aclk       : std_logic;
    signal m_axis_tready     : axis_tready_array_type(0 to Lanes-1);
    signal s_axis            : axis_64_array_type(0 to Lanes-1);
    signal s_axis_aclk       : std_logic;
    signal s_axis_tready     : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal s_axis_tready is never read"
    signal m_axis            : axis_64_array_type(0 to Lanes-1);        -- @suppress "signal m_axis is never read"
    signal m_axis_prog_empty : axis_tready_array_type(0 to Lanes-1);    -- @suppress "signal m_axis_prog_empty is never read"
    signal Decoder_lock      : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit-- @suppress "signal Decoder_lock is never read"
    signal HealthLane        : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal HealthLane is never read"
    signal HealthInterface   : std_logic_vector((Lanes/BondNumberOfLanes)-1 downto 0);
    signal Descrambler_lock  : std_logic_vector(Lanes-1 downto 0);      --TODO use as status bit -- @suppress "signal Descrambler_lock is never read"
    signal Channel           : std_logic_vector(7 downto 0);            --TODO use as status bit -- @suppress "signal Channel is never read"
    signal stat_rx_aligned   : STD_LOGIC;  

    signal tx_user_clk_out, rx_user_clk_out : std_logic;
    signal m_axis_burst, m_axis_deburst : axis_64_array_type(0 to Lanes - 1);
    
    signal latency_o : std_logic_vector(15 downto 0);
    signal valid_o   : std_logic_vector(lanes-1 downto 0);
    
    signal probe0 : std_logic_vector(63 DOWNTO 0); 
	signal probe1 : std_logic_vector(0 DOWNTO 0); 
	signal probe2 : std_logic_vector(0 DOWNTO 0); 
	signal probe3 : std_logic_vector(7 DOWNTO 0); 
	signal probe4 : std_logic_vector(63 DOWNTO 0); 
	signal probe5 : std_logic_vector(0 DOWNTO 0); 
	signal probe6 : std_logic_vector(0 DOWNTO 0);
	signal probe7 : std_logic_vector(7 DOWNTO 0);
	
	signal probe8 : std_logic_vector(63 DOWNTO 0); 
	signal probe9 : std_logic_vector(0 DOWNTO 0); 
	signal probe10 : std_logic_vector(0 DOWNTO 0); 
	signal probe11 : std_logic_vector(7 DOWNTO 0); 
	signal probe12 : std_logic_vector(63 DOWNTO 0); 
	signal probe13 : std_logic_vector(0 DOWNTO 0); 
	signal probe14 : std_logic_vector(0 DOWNTO 0);
	signal probe15 : std_logic_vector(7 DOWNTO 0);
	
	signal probe16 : std_logic_vector(0 DOWNTO 0); 
	signal probe17 : std_logic_vector(0 DOWNTO 0);
	
	signal probe18 : std_logic_vector(15 DOWNTO 0);
	signal probe19 : std_logic_vector(3 DOWNTO 0);
	
       COMPONENT vio_0
      PORT (
        clk : IN STD_LOGIC;
        probe_in0 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        probe_in1 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_in4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out1 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        probe_out2 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out3 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out4 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out5 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe_out6 : out std_logic_vector(0 downto 0)
      );
    END COMPONENT;
     component clk_wiz_1
    port
     (-- Clock in ports
      -- Clock out ports
      clk_100          : out    std_logic;
      clk_156          : out    std_logic;
      clk_in1_p         : in     std_logic;
      clk_in1_n         : in     std_logic
     );
    end component;

    signal loopback : std_logic_vector(2 downto 0);
    signal rxpmaresetdone_o: std_logic_vector(3 downto 0);
    signal txpmaresetdone_o: std_logic_vector(3 downto 0);
    signal reset_tx_pll_and_datapath_in : std_logic;
    signal reset_tx_datapath_in : std_logic;
    signal reset_rx_pll_and_datapath_in : std_logic;
    signal reset_rx_datapath_in : std_logic;
    signal reset_rx_cdr_stable_out : std_logic;
    signal reset_tx_done_out : std_logic;
    signal gtpowergood_out : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal drp_src : std_logic;
    signal clk_156, sys_rst : std_logic;


begin
    
    sys_clk : clk_wiz_1
   port map ( 
  -- Clock out ports  
   clk_100 => clk100,
   clk_156 => clk_156,
   -- Clock in ports
   clk_in1_p => SYS_CLK_300_P,
   clk_in1_n => SYS_CLK_300_N
 );

    ---- 100MHz clock DS to SE
    --IBUFDS_inst : IBUFDS
    --port map (
    --   O => clk100,      
    --   I => qdr4_clk_p,    
    --   IB => qdr4_clk_n    
    --);

    RX_In_N_s <= gt_rx_n;
    RX_In_P_s <= gt_rx_p;
    gt_tx_n    <= TX_Out_N_s;
    gt_tx_p    <= TX_Out_P_s;
    
	------- The Interlaken Interface -------
    interface : entity work.interlaken_interface
    generic map(
         BurstMax     => 256, --(Bytes)
         BurstShort   => 64, --(Bytes)
         PacketLength => 256, --(Packets)
         Lanes        => lanes,
         BondNumberOfLanes => BondNumberOfLanes,
         txlanes => 1,
         rxlanes => 1,
         CARD_TYPE => 128,
         GTREFCLKS => 1
    )
    port map(
            clk100 => clk100,
            reset  => sys_rst,
            GTREFCLK_IN_P(0) => qsfp4_clock_p,
            GTREFCLK_IN_N(0) => qsfp4_clock_n,
            tx_user_clk_out(0) => tx_user_clk_out, --only first lane for now
            rx_user_clk_out(0) => rx_user_clk_out,
            TX_Out_P => TX_Out_P_s,
            TX_Out_N => TX_Out_N_s,
            RX_In_P  => RX_In_P_s,
            RX_In_N  => RX_In_N_s,
            -- vio-- 
            rxpmaresetdone_o => rxpmaresetdone_o,
            txpmaresetdone_o => txpmaresetdone_o,
            reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in,
            reset_tx_datapath_in  => reset_tx_datapath_in,
            reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in,
            reset_rx_datapath_in => reset_rx_datapath_in,
            reset_rx_cdr_stable_out => reset_rx_cdr_stable_out,
            reset_tx_done_out => reset_tx_done_out, 
            gtpowergood_out => gtpowergood_out,
            drp_src => drp_src,

            TX_FlowControl => (others => (others => '0')),
            m_axis_burst  => m_axis_burst,
            m_axis_deburst  => m_axis_deburst,
            s_axis        => s_axis,
            s_axis_aclk   => s_axis_aclk,
            s_axis_tready => s_axis_tready,
            FlowControl => open,
            m_axis_aclk   => m_axis_aclk,
            m_axis        => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty,
            Decoder_Lock     => Decoder_lock,
            Descrambler_lock => Descrambler_lock,
            --Channel => Channel,
            loopback_in => loopback,
            HealthLane  => HealthLane,
            HealthInterface => HealthInterface
        );
        
        s_axis_aclk <= tx_user_clk_out; 
        m_axis_aclk <= tx_user_clk_out; 
        
    ---- Generates input data and interface signals ----
    generate_data : entity work.axis_data_generator
    generic map (
        lanes => lanes
    )
    port map (
		s_axis_aclk => s_axis_aclk,
        m_axis_aclk => m_axis_aclk,
        reset => sys_rst,
      
        s_axis => s_axis,
        s_axis_tready => s_axis_tready,

        m_axis            => m_axis,
        m_axis_tready     => m_axis_tready,
        m_axis_prog_empty => m_axis_prog_empty,
        
        latency_o  => latency_o,
        valid_o    => valid_o,
        
        HealthLane  => HealthLane,
        HealthInterface => HealthInterface(0)
    );
    
    -- Generate ILA for each lane to makes sure all lanes will be synthesized --
    --g_lanes: for i in 0 to Lanes-1 generate 
    --begin
    --    -- ADD ILA 
    --    axis_ila : ila_0
    --    PORT MAP (
    --        clk => s_axis_aclk,
    --        probe0 => s_axis(i).tdata,
    --        probe1(0) => s_axis(i).tvalid,
    --        probe2(0) => s_axis(i).tlast,
    --        probe3 => s_axis(i).tkeep,
    --        probe4 => m_axis(i).tdata, 
    --        probe5(0) =>  m_axis(i).tvalid, 
    --        probe6(0) => m_axis(i).tlast,
    --        probe7 => m_axis(i).tkeep, 
            
    --        probe8 => m_axis_burst(i).tdata, 
    --        probe9(0) => m_axis_burst(i).tvalid, 
    --        probe10(0) => m_axis_burst(i).tlast, 
    --        probe11 => m_axis_burst(i).tkeep, 
    --        probe12 => m_axis_deburst(i).tdata, 
    --        probe13(0) => m_axis_deburst(i).tvalid, 
    --        probe14(0) => m_axis_deburst(i).tlast, 
    --        probe15 => m_axis_deburst(i).tkeep, 
            
    --        probe16(0) => s_axis_tready(i),
    --        probe17(0) => m_axis_tready(i),
    --        probe18 => latency_o,
    --        probe19(3 downto 0) => valid_o(3 downto 0)
    --    );
    --end generate;

     vio_1 : vio_0
  PORT MAP (
    clk => clk100,
    probe_out0(0) => sys_rst,
    probe_out1 => loopback,
    probe_out2(0) => reset_tx_pll_and_datapath_in,
    probe_out3(0) => reset_tx_datapath_in,
    probe_out4(0) => reset_rx_pll_and_datapath_in,
    probe_out5(0) => reset_rx_datapath_in,
    probe_in0 => rxpmaresetdone_o,
    probe_in1 => txpmaresetdone_o,
    probe_in2(0) => reset_rx_cdr_stable_out,
    probe_in3(0) => reset_tx_done_out,
    probe_in4 => gtpowergood_out,
    probe_out6(0) => drp_src
  );

    
end architecture Test;
