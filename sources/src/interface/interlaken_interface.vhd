library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

library unisim;
use unisim.vcomponents.all;

entity interlaken_interface is
    generic(
        BurstMax     : positive := 256;      -- Configurable value of BurstMax
        BurstShort   : positive := 8;      -- Configurable value of BurstShort
        PacketLength : positive := 192;     -- Configurable value of PacketLength -- 24 packets * 8  = 192 B
        Lanes        : positive := 4;    -- Number of Lanes (Transmission channels)
        CLOCKING_MODE : string := "independent_clock";
        RELATED_CLOCKS : integer range 0 to 1 := 0;
        FIFO_MEMORY_TYPE : string := "auto";
        PACKET_FIFO : string := "false";
        txlanes : integer := 0;
        rxlanes : integer := 0;
        BondNumberOfLanes : positive := 1;
        CARD_TYPE    : integer := 128;
        GTREFCLKS    : integer := 1
    );
    port (
        reset : in std_logic;
        
        --------- 125 MHz input, to transceiver (QSFP4 clock)------------
        GTREFCLK_IN_P : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_IN_N : in std_logic_vector(GTREFCLKS-1 downto 0);
        
        -------- 100 MHz input, Free Running CLK (QDR4 clock) -----------
        clk100 : in std_logic;
        
        --- Debug signals (VIO)-----
        rxpmaresetdone_o: out std_logic_vector(3 downto 0);
        txpmaresetdone_o: out std_logic_vector(3 downto 0);
        reset_tx_pll_and_datapath_in : in std_logic;
        reset_tx_datapath_in : in std_logic;
        reset_rx_pll_and_datapath_in : in std_logic;
        reset_rx_datapath_in : in std_logic;
        reset_rx_cdr_stable_out : out std_logic;
        reset_tx_done_out : out std_logic;
        gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        drp_src : in std_logic;

        ------------------- GT data in/out ------------------------------
        TX_Out_P  : out std_logic_vector(Lanes-1 downto 0);
        TX_Out_N  : out std_logic_vector(Lanes-1 downto 0);
        RX_In_P   : in std_logic_vector(Lanes-1 downto 0);
        RX_In_N   : in std_logic_vector(Lanes-1 downto 0);
		
		----Transmitter input/ready signals--------------
        TX_FlowControl    : in slv_16_array(0 to Lanes-1);
        s_axis            : in axis_64_array_type(0 to Lanes-1);
        --s_axis_prog_empty : out axis_tready_array_type(0 to Lanes-1);
        s_axis_aclk       : in std_logic;
        s_axis_tready     : out axis_tready_array_type(0 to Lanes-1);

        ----Receiver output signals-----------------------
        FlowControl	      : out slv_16_array(0 to Lanes-1);     -- Flow control data (yet unutilized)
        m_axis_aclk       : in std_logic;
        m_axis            : out axis_64_array_type(0 to Lanes-1);
        m_axis_tready     : in axis_tready_array_type(0 to Lanes-1);
        m_axis_prog_empty : out axis_tready_array_type(0 to Lanes-1);
		
		--- Debug signals (direct core in and output data)-----
		m_axis_deburst    : out axis_64_array_type(0 to Lanes-1);
		m_axis_burst      : out axis_64_array_type(0 to Lanes-1);
        
        tx_user_clk_out   : out std_logic_vector(Lanes/4-1 downto 0);
        rx_user_clk_out   : out std_logic_vector(Lanes/4-1 downto 0);

        ------------------Receiver status signals-----------------------
        Decoder_Lock      : out std_logic_vector(Lanes-1 downto 0);
        Descrambler_lock  : out std_logic_vector(Lanes-1 downto 0);
        
        loopback_in       : in std_logic_vector(2 downto 0);
        HealthLane        : out std_logic_vector(Lanes-1 downto 0);
        HealthInterface   : out std_logic_vector((Lanes/BondNumberOfLanes)-1 downto 0)
		
	);
end entity interlaken_interface;

architecture interface of interlaken_interface is

    signal TX_User_Clock, RX_User_Clock : std_logic_vector(Lanes/4-1 downto 0);
    
    signal RX_Datavalid_Out : std_logic_vector(Lanes-1 downto 0);
    signal RX_Header_Out : slv_3_array(0 to Lanes-1);
    signal RX_Headervalid_Out : std_logic_vector(Lanes-1 downto 0);

    --signal RX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal RX_Resetdone_Out is never read"
    signal TX_Gearboxready_Out : std_logic_vector(Lanes-1 downto 0);
    signal TX_Header_In : slv_3_array(0 to Lanes-1);
    --signal TX_Resetdone_Out : std_logic_vector(Lanes-1 downto 0); --Todo use as status bit -- @suppress "signal TX_Resetdone_Out is never read"

    signal Data_Transceiver_In, Data_Transceiver_Out : slv_64_array(0 to Lanes-1);
    --signal HealthInterface_s :std_logic(Lanes-1 downto 0);--not used yet
    signal Descrambler_Lock_s, Descrambler_Lock_tx : std_logic_vector(Lanes-1 downto 0);

    signal TX_Data_out_s            : slv_67_array(0 to Lanes-1);
    signal RX_Data_In_s             : slv_67_array(0 to Lanes-1);

    signal rst_txusr_403M, rst_rxusr_403M : std_logic;

begin
    
    tx_user_clk_out <= TX_User_Clock;
    rx_user_clk_out <= RX_User_Clock;
        
    interlaken_gty_2 : entity work.interlaken_gty
        generic map (
            Lanes             => lanes,
            BondNumberOfLanes => BondNumberOfLanes,
            CARD_TYPE         => 129,
            GTREFCLKS         => GTREFCLKS
        )
        port map (
            reset                        => reset,
            rst_txusr_403M_s             => rst_txusr_403M,
            rst_rxusr_403M_s             => rst_rxusr_403M,
            GTREFCLK_IN_P                => GTREFCLK_IN_P,
            GTREFCLK_IN_N                => GTREFCLK_IN_N,
            clk100                       => clk100,
            rxpmaresetdone_o             => rxpmaresetdone_o,
            txpmaresetdone_o             => txpmaresetdone_o,
            reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in,
            reset_tx_datapath_in         => reset_tx_datapath_in,
            reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in,
            reset_rx_datapath_in         => reset_rx_datapath_in,
            reset_rx_cdr_stable_out      => reset_rx_cdr_stable_out,
            reset_tx_done_out            => reset_tx_done_out,
            gtpowergood_out              => gtpowergood_out,
            drp_src                      => drp_src,
            TX_Out_P                     => TX_Out_P,
            TX_Out_N                     => TX_Out_N,
            RX_In_P                      => RX_In_P,
            RX_In_N                      => RX_In_N,
            TX_User_Clock_s              => TX_User_Clock,
            RX_User_Clock_s              => RX_User_Clock,
            loopback_in                  => loopback_in,
            Data_Transceiver_In          => Data_Transceiver_In,
            Data_Transceiver_Out         => Data_Transceiver_Out,
            RX_Datavalid_Out             => RX_Datavalid_Out,
            RX_Header_Out_s              => RX_Header_Out,
            RX_Headervalid_Out_s         => RX_Headervalid_Out,
            TX_Gearboxready_Out          => TX_Gearboxready_Out,
            TX_Header_In                 => TX_Header_In
        );        
    g_unbonded_channels: for i in 0 to (Lanes/BondNumberOfLanes)-1 generate
        txlane: if txlanes > 0 generate
            ---------------------------- Transmitting side -----------------------------
            Interlaken_TX : entity work.Interlaken_Transmitter_multiChannel
            generic map(
                BurstMax => BurstMax, -- Configurable value of BurstMax
                BurstShort => BurstShort, -- Configurable value of BurstShort
                PacketLength => PacketLength, -- Configurable value of PacketLength
                Lanes => BondNumberOfLanes,
                CLOCKING_MODE => CLOCKING_MODE,
                RELATED_CLOCKS => RELATED_CLOCKS,
                FIFO_MEMORY_TYPE => FIFO_MEMORY_TYPE,
                PACKET_FIFO => PACKET_FIFO
            )
            port map (
                clk   => TX_User_Clock(i/4),
                reset => rst_txusr_403M, -- reset,
                TX_Data_Out     => TX_Data_out_s(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1), --Data_Transceiver_In(i)(63 downto 0), -- 64 bits
                TX_Gearboxready => TX_Gearboxready_Out((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                FlowControl     => TX_FlowControl(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                HealthLane      => Descrambler_Lock_tx((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
    
                m_axis_burst  => m_axis_burst(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                s_axis_aclk   => s_axis_aclk,
                s_axis        => s_axis(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                s_axis_tready => s_axis_tready(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1)
            );
        end generate;
        txlane_disabled: if txlanes = 0 generate
            TX_Data_out_s <= (others => (others => '0'));
            s_axis_tready <= (others => '0');
            m_axis_burst  <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
        end generate;
        
        rxlane: if rxlanes > 0 generate
            ---------------------------- Receiving side --------------------------------
            Interlaken_RX : entity work.Interlaken_Receiver_multiChannel
            generic map (
                PacketLength => PacketLength,
                Lanes => BondNumberOfLanes,
                CLOCKING_MODE => CLOCKING_MODE,
                RELATED_CLOCKS => RELATED_CLOCKS,
                FIFO_MEMORY_TYPE => FIFO_MEMORY_TYPE,
                PACKET_FIFO => PACKET_FIFO
            )
            port map (
                clk => RX_User_Clock(i/4),
                reset => rst_rxusr_403M, -- reset,
                RX_Data_In => RX_Data_In_s(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                FlowControl => FlowControl(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                RX_Datavalid => RX_Datavalid_Out((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                Bitslip => open, --Ignored, bitslip is handled by Transceiver_10g_64b67b_BLOCK_SYNC_SM
                m_axis_deburst    => m_axis_deburst(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                m_axis_aclk       => m_axis_aclk,
                m_axis            => m_axis(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                m_axis_tready     => m_axis_tready(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                m_axis_prog_empty => m_axis_prog_empty(i*BondNumberOfLanes to (i+1)*BondNumberOfLanes-1),
                
                Descrambler_lock => Descrambler_Lock_s((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                Decoder_Lock     => Decoder_Lock((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                HealthLane       => HealthLane((i+1)*BondNumberOfLanes-1 downto i*BondNumberOfLanes),
                HealthInterface  => HealthInterface(i)
            );
        end generate;
        rxlane_disabled: if rxlanes = 0 generate
            FlowControl <= (others => (others => '0'));
            m_axis_deburst <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
            m_axis  <=  (others => ((others => '0'), '0','0',(others => '0'),(others => '0'),(others => '0')));
            m_axis_prog_empty <= (others => '0');
            Descrambler_lock_s <= (others => '1'); --Set to 1 so the tx side wil just transmit
            Decoder_Lock <= (others => '0');
            HealthLane  <= (others => '0');
            HealthInterface <= (others => '0');
        end generate;

        
    end generate;

    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => lanes   -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => Descrambler_Lock_tx,
        dest_clk => TX_User_Clock(0), 
        src_clk => RX_User_Clock(0),
        src_in => Descrambler_lock_s 
    );
    
    g_lane_data: for i in 0 to Lanes-1 generate
        -- Map data from TX to the transceiver --
        Data_Transceiver_In(i) <= TX_Data_out_s(i)(63 downto 0);
        TX_Header_In(i) <= TX_Data_out_s(i)(66 downto 64);
        -- Map data from transceiver to RX --
        RX_Data_In_s(i)(63 downto 0) <= Data_Transceiver_Out(i);
        RX_Data_In_s(i)(66 downto 64) <= RX_Header_Out(i);
    end generate;

    Descrambler_lock <= Descrambler_lock_s; --Descrambler_Lock_s;

end architecture interface;
