// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Mon Jul 25 13:29:02 2022
// Host        : celeste.phy.bnl.gov running 64-bit CentOS Linux release 8.2.2004 (Core)
// Command     : write_verilog -force -mode synth_stub -rename_top ila_1 -prefix
//               ila_1_ ila_1_stub.v
// Design      : ila_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu13p-flga2577-2LV-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2020.2" *)
module ila_1(clk, trig_in, trig_in_ack, probe0, probe1, probe2, 
  probe3, probe4, probe5, probe6, probe7)
/* synthesis syn_black_box black_box_pad_pin="clk,trig_in,trig_in_ack,probe0[7:0],probe1[9:0],probe2[15:0],probe3[15:0],probe4[10:0],probe5[6:0],probe6[15:0],probe7[0:0]" */;
  input clk;
  input trig_in;
  output trig_in_ack;
  input [7:0]probe0;
  input [9:0]probe1;
  input [15:0]probe2;
  input [15:0]probe3;
  input [10:0]probe4;
  input [6:0]probe5;
  input [15:0]probe6;
  input [0:0]probe7;
endmodule
