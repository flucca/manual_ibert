-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Mon Jul 25 08:28:58 2022
-- Host        : celeste.phy.bnl.gov running 64-bit CentOS Linux release 8.2.2004 (Core)
-- Command     : write_vhdl -force -mode synth_stub -rename_top vio_1 -prefix
--               vio_1_ vio_1_stub.vhdl
-- Design      : vio_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu13p-flga2577-2LV-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity vio_1 is
  Port ( 
    clk : in STD_LOGIC;
    probe_in0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    probe_in1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in3 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in4 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    probe_out0 : out STD_LOGIC_VECTOR ( 9 downto 0 );
    probe_out1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    probe_out2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    probe_out3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    probe_out4 : out STD_LOGIC_VECTOR ( 0 to 0 );
    probe_out5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    probe_out6 : out STD_LOGIC_VECTOR ( 0 to 0 );
    probe_out7 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    probe_out8 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    probe_out9 : out STD_LOGIC_VECTOR ( 0 to 0 );
    probe_out10 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    probe_out11 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    probe_out12 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    probe_out13 : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );

end vio_1;

architecture stub of vio_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,probe_in0[15:0],probe_in1[0:0],probe_in2[0:0],probe_in3[0:0],probe_in4[5:0],probe_out0[9:0],probe_out1[15:0],probe_out2[0:0],probe_out3[0:0],probe_out4[0:0],probe_out5[1:0],probe_out6[0:0],probe_out7[11:0],probe_out8[11:0],probe_out9[0:0],probe_out10[8:0],probe_out11[1:0],probe_out12[4:0],probe_out13[3:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "vio,Vivado 2020.2";
begin
end;
