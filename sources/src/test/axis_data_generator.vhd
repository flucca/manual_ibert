library ieee, xpm;
use xpm.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.interlaken_package.all;
use work.axi_stream_package.all;

entity axis_data_generator is
  generic(
      lanes : positive := 1
  );
  Port ( 
      s_axis_aclk : in std_logic;
      m_axis_aclk : in std_logic;
      reset : in std_logic;
      
      s_axis : out axis_64_array_type(0 to Lanes-1);
      s_axis_tready : in axis_tready_array_type(0 to Lanes-1);

      m_axis            : in axis_64_array_type(0 to Lanes-1);
      m_axis_tready     : out axis_tready_array_type(0 to Lanes-1);
      m_axis_prog_empty : in axis_tready_array_type(0 to Lanes-1);
      
      latency_o : out std_logic_vector(15 downto 0);
      valid_o   : out std_logic_vector(lanes-1 downto 0);
      
      HealthLane : in std_logic_vector(lanes-1 downto 0);
      HealthInterface : in std_logic
  );
end axis_data_generator;

architecture Behavioral of axis_data_generator is
    signal count : integer;
    signal packet_num : integer;
    signal tdata_t1, tdata_t2 : std_logic_vector(63 downto 0);
    signal latency, latency_val : integer;
    signal packet_num_rx, count_rx : integer;
    signal valid : std_logic_vector(lanes-1 downto 0);
    signal HealthInterface_sync: std_logic;
begin

    xpm_cdc_single_inst : xpm_cdc_single
    generic map (
      DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
      INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    )
    port map (
      dest_out => HealthInterface_sync, -- 1-bit output: src_in synchronized to the destination clock domain. This output
                            -- is registered.
    
      dest_clk => s_axis_aclk, -- 1-bit input: Clock signal for the destination clock domain.
      src_clk => m_axis_aclk,   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
      src_in => HealthInterface      -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    );
    
    m_axis_tready <= (others => '1');
    
    generate_axis_data : process(reset, s_axis_aclk)
        variable do_count : std_logic;
        --variable count : integer;
    begin
        if reset = '1' then
            tdata_t1 <= (others => '0');
            
            for i in 0 to Lanes-1 loop
                s_axis(i).tvalid <= '0';
                s_axis(i).tlast <='0';
            end loop;
            count <= 1;
            packet_num <= 1;
        elsif rising_edge (s_axis_aclk) then
            if HealthInterface_sync = '1' then
                for i in 0 to Lanes-1 loop
                    s_axis(i).tvalid <= '1';
                    s_axis(i).tkeep <= (others => '0');
                    s_axis(i).tuser <= (others => '0');
                    s_axis(i).tid <= (others => '0');
                end loop;
                
                do_count := '0';
                if count < 30 then
                    do_count := '0';
                    for i in 0 to Lanes-1 loop
                        
                        if (s_axis_tready(i) = '1') then --and (HealthLane(i) = '1') then
                            s_axis(i).tlast <='0';
                            do_count := '1';
                            s_axis(i).tdata <= x"00000000" + packet_num & x"00000000" + count + i;
                            tdata_t1 <= x"00000000" + packet_num & x"00000000" + count;
                            if count = 29 then
                                s_axis(i).tlast <='1'; --Improve so it applies to all lanes!
                                count <= 1;
                                packet_num <= packet_num + 1;
                                do_count := '0';
                            end if;
				        end if;
				    end loop;
				    
				    if do_count = '1' then
				        count <= count + 1;
				    end if;
				end if;
				
		    end if;
        end if;
    end process;
    
    check_data : process(reset, s_axis_aclk) 
        variable packet_num_v, count_v : integer;
    begin
        if reset = '1' then
            packet_num_rx <= 1;
            count_rx <= 1;
            count_v := 0;
            packet_num_v := 0;
            valid <= (others => '0');
        elsif rising_edge (s_axis_aclk) then
            for i in 0 to Lanes-1 loop
                if m_axis(i).tvalid = '1' then
                    valid(i) <= '0';
                    count_v := to_integer(unsigned(m_axis(i).tdata(31 downto 0)));
                    packet_num_v := to_integer(unsigned(m_axis(i).tdata(63 downto 32)));
                    if count_v = count_rx + i and packet_num_v = packet_num_rx then 
                        valid(i) <= '1';
                    end if;
                    count_rx <= count_rx + 1;
                    if m_axis(i).tlast ='1' then
                        packet_num_rx <= packet_num_rx + 1;
                        count_rx <= 1;
                    end if;
                end if;
            end loop;
        end if;
    end process;
    
    valid_o <= valid;
    
    calc_latency : process (reset, s_axis_aclk) 
        variable start_c : std_logic;
    begin
        if reset = '1' then
            start_c := '0';
            latency <= 0;
            latency_val <= 0;
            tdata_t2 <= (others => '0');
        elsif rising_edge (s_axis_aclk) then
--            if tdata_t1(63 downto 32)= packet_num and (tdata_t1(31 downto 0) = x"00000001") and start_c = '0' then
--                tdata_t2 <= tdata_t1;
--                start_c := '1';
--            end if;
            
--            if start_c = '1' then
--                latency <= latency + 1;
--            end if;
            
--            if m_axis(0).tdata = tdata_t2 then
--                latency_val <= latency;
--                start_c := '0';
--                latency <= 0;
--            end if;
            
        end if;
    end process;
    
    latency_o <= std_logic_vector(to_unsigned(latency_val, 16));

end Behavioral;
