set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_false_path -from [get_pins -hierarchical -filter NAME=~*Probe_out_reg*/C]
#set_false_path -to [ get_pins -hierarchical -filter "NAME=~*Probe_in_reg*/D" ]



set_false_path -from [get_pins {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.gtwizard_ultrascale_0_i/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/reset_synchronizer_tx_done_inst/rst_in_out_reg/C}] -to [get_pins {vio_1/inst/PROBE_IN_INST/probe_in_reg_reg[9]/D}]
