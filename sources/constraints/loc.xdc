
set_property PACKAGE_PIN C29 [get_ports SYS_CLK_300_P]
set_property PACKAGE_PIN C30 [get_ports SYS_CLK_300_N]

set_property IOSTANDARD DIFF_HSTL_I_18 [get_ports SYS_CLK_300_P]
#create_clock -period 3.333 -name SYS_CLK_300_P [get_ports SYS_CLK_300_P]

set_property PACKAGE_PIN AT40 [get_ports qsfp4_clock_n]
set_property PACKAGE_PIN AT39 [get_ports qsfp4_clock_p]