
create_clock -period 2.560 -name qsfp_clock [get_ports qsfp4_clock_p]
#create_clock -period 3.333 -name SYS_CLK_300_P [get_ports SYS_CLK_300_P]


#connect_debug_port dbg_hub/clk [get_nets m_axis_aclk]

create_clock -period 9.999 -name VIRTUAL_clk_100_clk_wiz_1 -waveform {0.000 4.999}
create_clock -period 2.560 -name {VIRTUAL_rxoutclk_out[0]} -waveform {0.000 1.280}

create_clock -period 2.560 -name {txoutclk_out[0]} -waveform {0.000 1.280}
set_property C_CLK_INPUT_FREQ_HZ 100000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk100]
