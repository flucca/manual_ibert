proc FullWriteOfile {} {
    #33265 - full
    #16633 - half
    #8316  - quarter
    #4158  - octal
    set ofile [open "eyeScan" w]
    reset_hw_axi [get_hw_axis hw_axi_1]
    set i 0
    create_hw_axi_txn read_tx1 [get_hw_axis hw_axi_1] -type READ -address 0 -len 1 -force
    while {$i < 33265} {
     run_hw_axi [get_hw_axi_txns read_tx1] -quiet
     set rd_report [report_hw_axi_txn read_tx1 -w 64 -t u2]
     #if {$rd_report == 0} {break}
     puts $ofile $rd_report
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
     create_hw_axi_txn read_tx1 [get_hw_axis hw_axi_1] -type READ -address [format %08x $i] -len 1 -force
    }
    close $ofile
 }
 proc HalfWriteOfile {} {
    #33265 - full
    #16633 - half
    #8316  - quarter
    #4158  - octal
    set ofile [open "eyeScan" w]
    reset_hw_axi [get_hw_axis hw_axi_1]
    set i 0
    create_hw_axi_txn read_tx1 [get_hw_axis hw_axi_1] -type READ -address 0 -len 1 -force
    while {$i < 16633} {
     run_hw_axi [get_hw_axi_txns read_tx1] -quiet
     set rd_report [report_hw_axi_txn read_tx1 -w 64 -t u2]
     #if {$rd_report == 0} {break}
     puts $ofile $rd_report
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
     create_hw_axi_txn read_tx1 [get_hw_axis hw_axi_1] -type READ -address [format %08x $i] -len 1 -force
    }
    close $ofile
 }
 
 proc DblWriteOfile {} {
    #33265 - full
    #16633 - half
    #8316  - quarter
    #4158  - octal
    set ofile [open "eyeScan" w]
    reset_hw_axi [get_hw_axis hw_axi_1]
    set i 0
    create_hw_axi_txn read_tx1 [get_hw_axis hw_axi_1] -type READ -address 0 -len 1 -force
    while {$i < 66530} {
     run_hw_axi [get_hw_axi_txns read_tx1] -quiet
     set rd_report [report_hw_axi_txn read_tx1 -w 64 -t u2]
     #if {$rd_report == 0} {break}
     puts $ofile $rd_report
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
      incr i
     create_hw_axi_txn read_tx1 [get_hw_axis hw_axi_1] -type READ -address [format %08x $i] -len 1 -force
    }
    close $ofile
 }

proc drpWrite {addr din} {
    #set x [expr {$arg1 + $arg2}];
    #return $x
    #ADDR
    set_property OUTPUT_VALUE $addr [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpaddrReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpaddrReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    #DRPDI
    set_property OUTPUT_VALUE $din [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpdiReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpdiReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    #WRITE
    startgroup
    set_property OUTPUT_VALUE 1 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpWeCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpWeCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    endgroup
    startgroup
    set_property OUTPUT_VALUE 0 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpWeCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpWeCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
    endgroup
}

proc drpRead {addr} {
  #ADDR
  set_property OUTPUT_VALUE $addr [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpaddrReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpaddrReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  #READ
  startgroup
  set_property OUTPUT_VALUE 1 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpEnCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpEnCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  endgroup
  startgroup
  set_property OUTPUT_VALUE 0 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpEnCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpEnCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  endgroup
}

proc resetEyeScan {true} {
  startgroup
  set_property OUTPUT_VALUE $true [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/eyescanrst_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/eyescanrst_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  endgroup
}


proc readVio {probe} {
  get_property INPUT_VALUE  [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/$probe} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
}


proc go {} {
  startgroup
  set_property OUTPUT_VALUE 1 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/SOFRST} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/SOFRST} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  endgroup
  startgroup
  set_property OUTPUT_VALUE 0 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/SOFRST} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/SOFRST} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  endgroup
  startgroup
  set_property OUTPUT_VALUE 1 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/eyescanrst_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/eyescanrst_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  endgroup
  startgroup
  set_property OUTPUT_VALUE 0 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/eyescanrst_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/eyescanrst_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  endgroup
  startgroup
  set_property OUTPUT_VALUE 1 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/reset} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/reset} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]]
  endgroup
  startgroup
  set_property OUTPUT_VALUE 0 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/reset} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/reset} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]]
  endgroup

  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/hstepSize_v} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  set_property OUTPUT_VALUE 2 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  set_property OUTPUT_VALUE 0 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
}

proc init {} {
  set_property OUTPUT_VALUE 1 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  set_property OUTPUT_VALUE 0 [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
}

proc shorten {} {
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/DRPRDY_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/TXFSMRESETDONE} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/state_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/DRPDO_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/RXFSMRESETDONE} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpEnCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpaddrReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/eyescanrst_in} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/gty_select} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/numSteps} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/prescaleWire} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/vs_rangeWire} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/SOFRST} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpWeCmd} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpdiReg} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/go} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/hstepSize_v} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/rstRtl} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/vstepSize_v} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/data_i_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/addr_o} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/data_vio_o} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/ram_src_slct_o} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/reset} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/we_o} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/out_mem/design/design_2_i/vio_0"}]] 
  set_property NAME.SELECT short [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/diff_swing} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]] 

}


proc EyeScan {horz vert} {
  drpWrite 03C 0304
  #Horizontal
  drpWrite 04F $horz
  #Vertical
  drpWrite 097 $vert
  #Run
  drpWrite 03C 0704
  #Read control_es
  drpRead 253
  drpWrite 03C 0304
}

proc run {horz vert range} {
  
  
  set_property OUTPUT_VALUE $horz [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/hstepSize_v} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/hstepSize_v} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]

  set_property OUTPUT_VALUE $vert [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/vstepSize_v} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/vstepSize_v} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  
  set_property OUTPUT_VALUE $range [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/vs_rangeWire} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  commit_hw_vio [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/vs_rangeWire} -of_objects [get_hw_vios -of_objects [get_hw_devices xcvu13p_0] -filter {CELL_NAME=~"interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/drpCntrlIns"}]]
  
  go
  set state [get_property INPUT_VALUE [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/state_1}]]
  while {$state == {00}} {
    set state [get_property INPUT_VALUE [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/state_1}]]
    
  }
  while {$state != 00} {
    set state [get_property INPUT_VALUE [get_hw_probes {interface/Interlaken_gty/g_quads[0].g_ultrascale_debug.man_ibert/state_1}]]
  }
  
  FullWriteOfile
}