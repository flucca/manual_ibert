import os
import argparse
import plot_eyescan
def parseArguments():
    # Create argument parser
    parser = argparse.ArgumentParser()

    # Positional mandatory arguments
    parser.add_argument("horz", help="Horizontal step.", type=int)
    parser.add_argument("vert", help="Vertical step.", type=int)
    parser.add_argument("vrange", help="Vertical range (0-3).", type=int)

    # Optional arguments
    parser.add_argument("-s", "--swing", help="Tx vertical swing.", type=int, default=00.)
    parser.add_argument("-f", "--file", help="Output file.", default="GCM/eyeScan")

    # Print version
    # parser.add_argument("--version", action="version", version='%(prog)s - Version 1.0')

    # Parse arguments
    args = parser.parse_args()

    return args

args = parseArguments()


query = "SetSwing {{}}".format(args.swing)
os.system("echo {} | nc 127.0.0.1 8020".format(query))

query = "run {{}} {{}} {{}}".format(args.horz, args.vert, args.vrange)
os.system("echo {} | nc 127.0.0.1 8020".format(query))
if(args.horz == 1 and args.vert == 1):
    query = "DblWriteOfile".format(args.swing)
    os.system("echo {} | nc 127.0.0.1 8020".format(query))
plot_eyescan(args.file)
# query = "exit"