# Manual Ibert

Implementation of a manual in system eye scan for GTY in Ultrascale+

### Based on an example design by Xilinx
https://support.xilinx.com/s/article/64098?language=en_US

### And the Core1990 implementation of interlaken


## Structure

    -interlaken_top
        -interlaken_interface
            -interlaken_gty
                -gty_inst
                -In-system IBERT
                -manual eye scan
                    -out memory ctrl
            -transmitter
            -receiver
        -data_generator

## Scripts

**scripts.tcl** holds all the tcl procs relevant to the eye scan.

The main tcl proc is `run <horizontal step> <vertical step> <vertical range>`, it runs the eye scan and reads the output memory writing to a file called eyeScan.
```
Example:
run 002 002 2
```
After which plot_eyescan.py3 plots the heat map from the ouput file.

The output file could be incomplete if the highest resolution is used (horz = 1, vert = 1) since run calls the FullWriteOfile proc which reads half the total RAM address space (this is done to speed up the process). If the highest resolution is used `DblWriteOfile` should be executed to retrieve the whole eye scan from the RAM. *The run_eyescan.py script does this automatically*

The tcl scripts read and write to the vio in the device called "xcvu13p_0". To make use of the second device "xcvu13p_1", the **scripts_1.tcl** should be sourced instead.
