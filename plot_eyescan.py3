import os, sys, time, datetime, csv
import numpy as np
import re
import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cbook as cbook
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
import pandas as pd
from struct import *
from  matplotlib.colors import LinearSegmentedColormap
cmap=LinearSegmentedColormap.from_list('rg',["b", "lightblue", "lightgreen", "y", "orange", "r", "darkred"], N=256) 
file_name = sys.argv[1]
PRESCALE = 4#sys.argv[2]
#fo = open(file_name, "r+")
p = re.compile('\w+')
datapoints_list = []
with open(file_name, 'r') as csv_file:
    # my_reader = csv.DictReader(csv_file)
    for row in csv_file:#my_reader:
        if (row != "\n"):
            datapoints_list.append(row)

row_numbers = []
ber_list = []
horz_list = []
vert_list = []
vs_reg = []
hz_reg = []
array = np.empty([65,32], dtype = float)
labels = np.empty([65,32], dtype = int)
open_area = float(0)
unit_percentage = float(0)
THRESHOLD = float(1/100000)
for i in range(len(datapoints_list)):#range(0,4094):
    # print(datapoints_list[i])
    row_numbers =  p.findall(datapoints_list[i])
    errors = float(row_numbers[1]) #??
    samples = float(row_numbers[2])
    horz = int(row_numbers[3])
    vert = int(row_numbers[4])
    if (errors == 0 and samples == 0 and horz != 0 and vert != 0):
        # Header, holds info on run
        hz_reg = unpack('BB',horz.to_bytes(2,'little'))
        vs_reg = unpack('BB',vert.to_bytes(2,'little'))
        horz_step = hz_reg[0]
        PRESCALE = hz_reg[1] % 32
        vert_step = vs_reg[1]
        vert_range = vs_reg[0] % 4
        unit_percentage = 100*float(vert_step*horz_step)/16445
        if (vert_range == 0):
            rang = 1.6
        elif (vert_range == 1):
            rang = 2.0
        elif(vert_range == 2):
            rang = 2.4
        elif(vert_range == 3):
            rang = 3.3
        else:
            print("error !_!")
        continue
    if (vert % 2 == 1):
        vert = vert >> 1;
        vert = -vert;
    else:
        vert = vert >> 1;
    if (vert == horz == errors == samples == 0 and i > 1):
        break

    if (horz > 511):
        horz += 4294965248
        # if (horz & (1 << (31))) != 0: # if sign bit is set e.g., 8bit: 128-255
        horz = horz - (1 << 32)        # compute negative value

    if (samples != 0):

        if (errors != 0):
            BER = errors/(samples*pow(2,(1+PRESCALE))*2)
        else:
            BER = 1/(samples*pow(2,(1+PRESCALE))*2)
        if (BER < THRESHOLD):
            open_area += unit_percentage
    #np[horz,vert] = BER
        # print("BER: ",BER)
        ber_list.append(BER)
        #array[horz,vert_index] = BER
        horz_list.append(horz*0.015625)
        vert_list.append(round(vert*rang))
    if (vert == -124):
        break
df = pd.DataFrame({'Unit Interval': horz_list,
                   'Vertical Codes [mV]': vert_list,
                   'BER': ber_list})
                   
# print(df)
result = df.pivot(index='Vertical Codes [mV]',columns='Unit Interval',values='BER')
# print(result)

# PLOT
fig, a = plt.subplots(figsize=(15,9))
# fig.suptitle("TITLE")

bermin = min(ber_list)
bermax = max(ber_list)
# try:
a = sns.heatmap(result,annot=False,cbar=False,ax=a)
# except Exception as e:
    # try:
        # a = sns.heatmap(result,annot=False,cmap='rocket_r',center=result.loc[2,0],vmin=bermin,vmax=bermax )#linewidths=0.30,ax=ax)
    # except Exception as e:
        # a = sns.heatmap(result,annot=False,cmap='rocket_r',center=result.loc[0,2],vmin=bermin,vmax=bermax )#linewidths=0.30,ax=ax)
text = "Vertical Range = {}mv/step; Step size = {}; Vertical step = {}; Dwell = 10^-{}"
other_text = "Open area = {}%"
font = {'family': 'sans-serif',
        'color':  'white',
        'weight': 'normal',
        'size': 16,
        }
a.text(26/horz_step,126/vert_step,other_text.format(round(open_area,2)),fontdict=font)

ax = plt.gca()
ax.invert_yaxis()
a.set_title(text.format(rang,horz_step,vert_step, PRESCALE+1),y=1.03)
pcm = a.pcolor(result, norm=colors.LogNorm(vmin=bermin, vmax=bermax), cmap=cmap, shading='auto')
fig.colorbar(pcm, extend='max')
plt.show()
