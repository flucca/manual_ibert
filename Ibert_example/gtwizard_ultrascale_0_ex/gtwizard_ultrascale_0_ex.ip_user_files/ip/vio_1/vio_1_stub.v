// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Thu Mar  9 11:45:55 2023
// Host        : celeste.phy.bnl.gov running 64-bit CentOS Linux release 8.5.2111
// Command     : write_verilog -force -mode synth_stub
//               /DATA/users/flucca/Core1990/test/Ibert_example/gtwizard_ultrascale_0_ex/imports/vio_test/vio_1_stub.v
// Design      : vio_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xczu19eg-ffvd1760-1-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module vio_1(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_out0, probe_out1, probe_out2, probe_out3, probe_out4, probe_out5, probe_out6, 
  probe_out7, probe_out8, probe_out9, probe_out10, probe_out11, probe_out12, probe_out13, 
  probe_out14)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[15:0],probe_in1[0:0],probe_in2[0:0],probe_in3[0:0],probe_in4[7:0],probe_out0[9:0],probe_out1[15:0],probe_out2[0:0],probe_out3[0:0],probe_out4[0:0],probe_out5[1:0],probe_out6[0:0],probe_out7[11:0],probe_out8[11:0],probe_out9[0:0],probe_out10[8:0],probe_out11[1:0],probe_out12[4:0],probe_out13[1:0],probe_out14[5:0]" */;
  input clk;
  input [15:0]probe_in0;
  input [0:0]probe_in1;
  input [0:0]probe_in2;
  input [0:0]probe_in3;
  input [7:0]probe_in4;
  output [9:0]probe_out0;
  output [15:0]probe_out1;
  output [0:0]probe_out2;
  output [0:0]probe_out3;
  output [0:0]probe_out4;
  output [1:0]probe_out5;
  output [0:0]probe_out6;
  output [11:0]probe_out7;
  output [11:0]probe_out8;
  output [0:0]probe_out9;
  output [8:0]probe_out10;
  output [1:0]probe_out11;
  output [4:0]probe_out12;
  output [1:0]probe_out13;
  output [5:0]probe_out14;
endmodule
