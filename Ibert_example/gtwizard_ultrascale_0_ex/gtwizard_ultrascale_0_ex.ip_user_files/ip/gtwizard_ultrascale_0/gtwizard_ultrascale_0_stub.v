// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Mar 17 15:20:35 2023
// Host        : celeste.phy.bnl.gov running 64-bit CentOS Linux release 8.5.2111
// Command     : write_verilog -force -mode synth_stub
//               /DATA/users/flucca/Core1990/test/Ibert_example/gtwizard_ultrascale_0_ex/gtwizard_ultrascale_0_ex.gen/sources_1/ip/gtwizard_ultrascale_0/gtwizard_ultrascale_0_stub.v
// Design      : gtwizard_ultrascale_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xczu19eg-ffvd1760-1-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "gtwizard_ultrascale_0_gtwizard_top,Vivado 2020.2" *)
module gtwizard_ultrascale_0(gtwiz_userclk_tx_active_in, 
  gtwiz_userclk_rx_active_in, gtwiz_reset_clk_freerun_in, gtwiz_reset_all_in, 
  gtwiz_reset_tx_pll_and_datapath_in, gtwiz_reset_tx_datapath_in, 
  gtwiz_reset_rx_pll_and_datapath_in, gtwiz_reset_rx_datapath_in, 
  gtwiz_reset_rx_cdr_stable_out, gtwiz_reset_tx_done_out, gtwiz_reset_rx_done_out, 
  gtwiz_userdata_tx_in, gtwiz_userdata_rx_out, gtrefclk00_in, qpll0lock_out, 
  qpll0outclk_out, qpll0outrefclk_out, drpaddr_in, drpclk_in, drpdi_in, drpen_in, drpwe_in, 
  eyescanreset_in, gtyrxn_in, gtyrxp_in, loopback_in, rxgearboxslip_in, rxlpmen_in, rxrate_in, 
  rxusrclk_in, rxusrclk2_in, txdiffctrl_in, txheader_in, txpostcursor_in, txprecursor_in, 
  txsequence_in, txusrclk_in, txusrclk2_in, drpdo_out, drprdy_out, gtpowergood_out, gtytxn_out, 
  gtytxp_out, rxdatavalid_out, rxheader_out, rxheadervalid_out, rxoutclk_out, 
  rxpmaresetdone_out, rxstartofseq_out, txoutclk_out, txpmaresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[255:0],gtwiz_userdata_rx_out[255:0],gtrefclk00_in[0:0],qpll0lock_out[0:0],qpll0outclk_out[0:0],qpll0outrefclk_out[0:0],drpaddr_in[39:0],drpclk_in[3:0],drpdi_in[63:0],drpen_in[3:0],drpwe_in[3:0],eyescanreset_in[3:0],gtyrxn_in[3:0],gtyrxp_in[3:0],loopback_in[11:0],rxgearboxslip_in[3:0],rxlpmen_in[3:0],rxrate_in[11:0],rxusrclk_in[3:0],rxusrclk2_in[3:0],txdiffctrl_in[19:0],txheader_in[23:0],txpostcursor_in[19:0],txprecursor_in[19:0],txsequence_in[27:0],txusrclk_in[3:0],txusrclk2_in[3:0],drpdo_out[63:0],drprdy_out[3:0],gtpowergood_out[3:0],gtytxn_out[3:0],gtytxp_out[3:0],rxdatavalid_out[7:0],rxheader_out[23:0],rxheadervalid_out[7:0],rxoutclk_out[3:0],rxpmaresetdone_out[3:0],rxstartofseq_out[7:0],txoutclk_out[3:0],txpmaresetdone_out[3:0]" */;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [255:0]gtwiz_userdata_tx_in;
  output [255:0]gtwiz_userdata_rx_out;
  input [0:0]gtrefclk00_in;
  output [0:0]qpll0lock_out;
  output [0:0]qpll0outclk_out;
  output [0:0]qpll0outrefclk_out;
  input [39:0]drpaddr_in;
  input [3:0]drpclk_in;
  input [63:0]drpdi_in;
  input [3:0]drpen_in;
  input [3:0]drpwe_in;
  input [3:0]eyescanreset_in;
  input [3:0]gtyrxn_in;
  input [3:0]gtyrxp_in;
  input [11:0]loopback_in;
  input [3:0]rxgearboxslip_in;
  input [3:0]rxlpmen_in;
  input [11:0]rxrate_in;
  input [3:0]rxusrclk_in;
  input [3:0]rxusrclk2_in;
  input [19:0]txdiffctrl_in;
  input [23:0]txheader_in;
  input [19:0]txpostcursor_in;
  input [19:0]txprecursor_in;
  input [27:0]txsequence_in;
  input [3:0]txusrclk_in;
  input [3:0]txusrclk2_in;
  output [63:0]drpdo_out;
  output [3:0]drprdy_out;
  output [3:0]gtpowergood_out;
  output [3:0]gtytxn_out;
  output [3:0]gtytxp_out;
  output [7:0]rxdatavalid_out;
  output [23:0]rxheader_out;
  output [7:0]rxheadervalid_out;
  output [3:0]rxoutclk_out;
  output [3:0]rxpmaresetdone_out;
  output [7:0]rxstartofseq_out;
  output [3:0]txoutclk_out;
  output [3:0]txpmaresetdone_out;
endmodule
