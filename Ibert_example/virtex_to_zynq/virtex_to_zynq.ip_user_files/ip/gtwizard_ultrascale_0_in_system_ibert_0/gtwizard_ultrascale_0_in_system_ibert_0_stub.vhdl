-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Fri Mar 17 15:29:41 2023
-- Host        : celeste.phy.bnl.gov running 64-bit CentOS Linux release 8.5.2111
-- Command     : write_vhdl -force -mode synth_stub
--               /DATA/users/flucca/Core1990/test/Ibert_example/virtex_to_zynq/virtex_to_zynq.gen/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/gtwizard_ultrascale_0_in_system_ibert_0_stub.vhdl
-- Design      : gtwizard_ultrascale_0_in_system_ibert_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu13p-flga2577-2LV-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gtwizard_ultrascale_0_in_system_ibert_0 is
  Port ( 
    drpclk_o : out STD_LOGIC_VECTOR ( 3 downto 0 );
    gt0_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt0_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt1_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt1_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt1_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt1_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt1_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt1_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt2_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt2_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt2_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt2_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt2_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt2_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt3_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt3_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt3_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt3_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt3_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt3_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    eyescanreset_o : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rxrate_o : out STD_LOGIC_VECTOR ( 11 downto 0 );
    txdiffctrl_o : out STD_LOGIC_VECTOR ( 19 downto 0 );
    txprecursor_o : out STD_LOGIC_VECTOR ( 19 downto 0 );
    txpostcursor_o : out STD_LOGIC_VECTOR ( 19 downto 0 );
    rxlpmen_o : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rxoutclk_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    clk : in STD_LOGIC
  );

end gtwizard_ultrascale_0_in_system_ibert_0;

architecture stub of gtwizard_ultrascale_0_in_system_ibert_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "drpclk_o[3:0],gt0_drpen_o[0:0],gt0_drpwe_o[0:0],gt0_drpaddr_o[9:0],gt0_drpdi_o[15:0],gt0_drprdy_i[0:0],gt0_drpdo_i[15:0],gt1_drpen_o[0:0],gt1_drpwe_o[0:0],gt1_drpaddr_o[9:0],gt1_drpdi_o[15:0],gt1_drprdy_i[0:0],gt1_drpdo_i[15:0],gt2_drpen_o[0:0],gt2_drpwe_o[0:0],gt2_drpaddr_o[9:0],gt2_drpdi_o[15:0],gt2_drprdy_i[0:0],gt2_drpdo_i[15:0],gt3_drpen_o[0:0],gt3_drpwe_o[0:0],gt3_drpaddr_o[9:0],gt3_drpdi_o[15:0],gt3_drprdy_i[0:0],gt3_drpdo_i[15:0],eyescanreset_o[3:0],rxrate_o[11:0],txdiffctrl_o[19:0],txprecursor_o[19:0],txpostcursor_o[19:0],rxlpmen_o[3:0],rxoutclk_i[3:0],clk";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "in_system_ibert,Vivado 2020.2";
begin
end;
