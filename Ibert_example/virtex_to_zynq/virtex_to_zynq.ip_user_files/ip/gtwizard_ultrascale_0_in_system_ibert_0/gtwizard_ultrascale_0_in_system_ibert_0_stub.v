// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Mar 17 15:29:40 2023
// Host        : celeste.phy.bnl.gov running 64-bit CentOS Linux release 8.5.2111
// Command     : write_verilog -force -mode synth_stub
//               /DATA/users/flucca/Core1990/test/Ibert_example/virtex_to_zynq/virtex_to_zynq.gen/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/gtwizard_ultrascale_0_in_system_ibert_0_stub.v
// Design      : gtwizard_ultrascale_0_in_system_ibert_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu13p-flga2577-2LV-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "in_system_ibert,Vivado 2020.2" *)
module gtwizard_ultrascale_0_in_system_ibert_0(drpclk_o, gt0_drpen_o, gt0_drpwe_o, 
  gt0_drpaddr_o, gt0_drpdi_o, gt0_drprdy_i, gt0_drpdo_i, gt1_drpen_o, gt1_drpwe_o, 
  gt1_drpaddr_o, gt1_drpdi_o, gt1_drprdy_i, gt1_drpdo_i, gt2_drpen_o, gt2_drpwe_o, 
  gt2_drpaddr_o, gt2_drpdi_o, gt2_drprdy_i, gt2_drpdo_i, gt3_drpen_o, gt3_drpwe_o, 
  gt3_drpaddr_o, gt3_drpdi_o, gt3_drprdy_i, gt3_drpdo_i, eyescanreset_o, rxrate_o, 
  txdiffctrl_o, txprecursor_o, txpostcursor_o, rxlpmen_o, rxoutclk_i, clk)
/* synthesis syn_black_box black_box_pad_pin="drpclk_o[3:0],gt0_drpen_o[0:0],gt0_drpwe_o[0:0],gt0_drpaddr_o[9:0],gt0_drpdi_o[15:0],gt0_drprdy_i[0:0],gt0_drpdo_i[15:0],gt1_drpen_o[0:0],gt1_drpwe_o[0:0],gt1_drpaddr_o[9:0],gt1_drpdi_o[15:0],gt1_drprdy_i[0:0],gt1_drpdo_i[15:0],gt2_drpen_o[0:0],gt2_drpwe_o[0:0],gt2_drpaddr_o[9:0],gt2_drpdi_o[15:0],gt2_drprdy_i[0:0],gt2_drpdo_i[15:0],gt3_drpen_o[0:0],gt3_drpwe_o[0:0],gt3_drpaddr_o[9:0],gt3_drpdi_o[15:0],gt3_drprdy_i[0:0],gt3_drpdo_i[15:0],eyescanreset_o[3:0],rxrate_o[11:0],txdiffctrl_o[19:0],txprecursor_o[19:0],txpostcursor_o[19:0],rxlpmen_o[3:0],rxoutclk_i[3:0],clk" */;
  output [3:0]drpclk_o;
  output [0:0]gt0_drpen_o;
  output [0:0]gt0_drpwe_o;
  output [9:0]gt0_drpaddr_o;
  output [15:0]gt0_drpdi_o;
  input [0:0]gt0_drprdy_i;
  input [15:0]gt0_drpdo_i;
  output [0:0]gt1_drpen_o;
  output [0:0]gt1_drpwe_o;
  output [9:0]gt1_drpaddr_o;
  output [15:0]gt1_drpdi_o;
  input [0:0]gt1_drprdy_i;
  input [15:0]gt1_drpdo_i;
  output [0:0]gt2_drpen_o;
  output [0:0]gt2_drpwe_o;
  output [9:0]gt2_drpaddr_o;
  output [15:0]gt2_drpdi_o;
  input [0:0]gt2_drprdy_i;
  input [15:0]gt2_drpdo_i;
  output [0:0]gt3_drpen_o;
  output [0:0]gt3_drpwe_o;
  output [9:0]gt3_drpaddr_o;
  output [15:0]gt3_drpdi_o;
  input [0:0]gt3_drprdy_i;
  input [15:0]gt3_drpdo_i;
  output [3:0]eyescanreset_o;
  output [11:0]rxrate_o;
  output [19:0]txdiffctrl_o;
  output [19:0]txprecursor_o;
  output [19:0]txpostcursor_o;
  output [3:0]rxlpmen_o;
  input [3:0]rxoutclk_i;
  input clk;
endmodule
