vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xpm
vlib questa_lib/msim/in_system_ibert_v1_0_12
vlib questa_lib/msim/xil_defaultlib

vmap xpm questa_lib/msim/xpm
vmap in_system_ibert_v1_0_12 questa_lib/msim/in_system_ibert_v1_0_12
vmap xil_defaultlib questa_lib/msim/xil_defaultlib

vlog -work xpm -64 -sv "+incdir+../../../../virtex_to_zynq.gen/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"/DATA/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/DATA/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/DATA/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work in_system_ibert_v1_0_12 -64 "+incdir+../../../../virtex_to_zynq.gen/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"../../../ipstatic/hdl/in_system_ibert_v1_0_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../virtex_to_zynq.gen/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"../../../../virtex_to_zynq.gen/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/sim/gtwizard_ultrascale_0_in_system_ibert_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

